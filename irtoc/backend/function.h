/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_FUNCTION_H
#define PANDA_FUNCTION_H

#include "compiler/optimizer/ir/ir_constructor.h"
#include "compiler/optimizer/code_generator/relocations.h"
#include "utils/expected.h"
#include "source_languages.h"

namespace panda::irtoc {

class Function : public compiler::RelocationHandler {
public:
    using Result = Expected<int, const char *>;

    Function() = default;

    virtual ~Function() = default;

    virtual void MakeGraphImpl() = 0;
    virtual const char *GetName() const = 0;

    Result Compile(Arch arch, ArenaAllocator *allocator, ArenaAllocator *local_allocator);

    auto GetCode() const
    {
        return Span(code_);
    }

    compiler::Graph *GetGraph()
    {
        return graph_;
    }

    const compiler::Graph *GetGraph() const
    {
        return graph_;
    }

    size_t WordSize() const
    {
        return PointerSize(GetArch());
    }

    void AddRelocation(const compiler::RelocationInfo &info) override;

    const auto &GetRelocations() const
    {
        return relocation_entries_;
    }

    const char *GetExternalFunction(size_t index) const
    {
        CHECK_LT(index, external_functions_.size());
        return external_functions_[index].c_str();
    }

    SourceLanguage GetLanguage() const
    {
        return lang_;
    }

    uint32_t AddSourceDir(std::string_view dir)
    {
        auto it = std::find(source_dirs_.begin(), source_dirs_.end(), dir);
        if (it != source_dirs_.end()) {
            return std::distance(source_dirs_.begin(), it);
        }
        source_dirs_.emplace_back(dir);
        return source_dirs_.size() - 1;
    }

    uint32_t AddSourceFile(std::string_view filename)
    {
        auto it = std::find(source_files_.begin(), source_files_.end(), filename);
        if (it != source_files_.end()) {
            return std::distance(source_files_.begin(), it);
        }
        source_files_.emplace_back(filename);
        return source_files_.size() - 1;
    }

    uint32_t GetSourceFileIndex(const char *filename) const
    {
        auto it = std::find(source_files_.begin(), source_files_.end(), filename);
        ASSERT(it != source_files_.end());
        return std::distance(source_files_.begin(), it);
    }

    uint32_t GetSourceDirIndex(const char *dir) const
    {
        auto it = std::find(source_dirs_.begin(), source_dirs_.end(), dir);
        ASSERT(it != source_dirs_.end());
        return std::distance(source_dirs_.begin(), it);
    }

    Span<const std::string> GetSourceFiles() const
    {
        return Span<const std::string>(source_files_);
    }

    Span<const std::string> GetSourceDirs() const
    {
        return Span<const std::string>(source_dirs_);
    }

protected:
    Arch GetArch() const
    {
        return GetGraph()->GetArch();
    }

    compiler::RuntimeInterface *GetRuntime()
    {
        return GetGraph()->GetRuntime();
    }

    void SetExternalFunctions(std::initializer_list<std::string> funcs)
    {
        external_functions_ = funcs;
    }

    void SetLanguage(SourceLanguage lang)
    {
        lang_ = lang;
    }

protected:
    std::unique_ptr<compiler::IrConstructor> builder_;

private:
    compiler::Graph *graph_ {nullptr};
    SourceLanguage lang_ {SourceLanguage::PANDA_ASSEMBLY};
    std::vector<uint8_t> code_;
    std::vector<std::string> external_functions_;
    std::vector<std::string> source_dirs_;
    std::vector<std::string> source_files_;
    std::vector<compiler::RelocationInfo> relocation_entries_;
};
}  // namespace panda::irtoc

#endif  // PANDA_FUNCTION_H
