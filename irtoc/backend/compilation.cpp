/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "compilation.h"
#include "function.h"
#include "mem/pool_manager.h"
#include "elfio/elfio.hpp"

#ifdef PANDA_COMPILER_DEBUG_INFO
#include "dwarf_builder.h"
#endif

namespace panda::irtoc {
// elfio library missed some elf constants, so lets define it here for a while. We can't include elf.h header because
// it conflicts with elfio.
static constexpr size_t EF_ARM_EABI_VER5 = 0x05000000;
static constexpr size_t EM_AARCH64 = 183;

Compilation::Result Compilation::Run(std::string_view output)
{
    if (compiler::OPTIONS.WasSetCompilerRegex()) {
        methods_regex_ = compiler::OPTIONS.GetCompilerRegex();
    }

    PoolManager::Initialize(PoolType::MALLOC);

    allocator_ = std::make_unique<ArenaAllocator>(SpaceType::SPACE_TYPE_COMPILER);
    local_allocator_ = std::make_unique<ArenaAllocator>(SpaceType::SPACE_TYPE_COMPILER);

    if (RUNTIME_ARCH == Arch::X86_64 && compiler::OPTIONS.WasSetCompilerCrossArch()) {
        arch_ = GetArchFromString(compiler::OPTIONS.GetCompilerCrossArch());
        if (arch_ == Arch::NONE) {
            LOG(FATAL, IRTOC) << "FATAL: unknown arch: " << compiler::OPTIONS.GetCompilerCrossArch();
        }
        compiler::OPTIONS.AdjustCpuFeatures(arch_ != RUNTIME_ARCH);
    } else {
        compiler::OPTIONS.AdjustCpuFeatures(false);
    }

    LOG(INFO, IRTOC) << "Start Irtoc compilation for " << GetArchString(arch_) << "...";

    auto result = Compile();
    if (result) {
        LOG(INFO, IRTOC) << "Irtoc compilation success";
    } else {
        LOG(ERROR, IRTOC) << "Irtoc compilation failed: " << result.Error();
    }

    if (result = MakeElf(output); !result) {
        return result;
    }

    for (auto unit : units_) {
        delete unit;
    }

    allocator_.reset();
    local_allocator_.reset();

    PoolManager::Finalize();

    return result;
}

Compilation::Result Compilation::Compile()
{
    for (auto unit : units_) {
        if (compiler::OPTIONS.WasSetCompilerRegex() && !std::regex_match(unit->GetName(), methods_regex_)) {
            continue;
        }
        LOG(INFO, IRTOC) << "Compile " << unit->GetName();
        auto result = unit->Compile(arch_, allocator_.get(), local_allocator_.get());
        if (!result) {
            return result;
        }
#ifdef PANDA_COMPILER_DEBUG_INFO
        has_debug_info_ |= unit->GetGraph()->IsLineDebugInfoEnabled();
#endif
    }

    return 0;
}

static size_t GetElfArch(Arch arch)
{
    switch (arch) {
        case Arch::AARCH32:
            return EM_ARM;
        case Arch::AARCH64:
            return EM_AARCH64;
        case Arch::X86:
            return EM_386;
        case Arch::X86_64:
            return EM_X86_64;
        default:
            UNREACHABLE();
    }
}

// CODECHECK-NOLINTNEXTLINE(C_RULE_ID_FUNCTION_SIZE)
Compilation::Result Compilation::MakeElf(std::string_view output)
{
    ELFIO::elfio elf_writer;
    elf_writer.create(Is64BitsArch(arch_) ? ELFCLASS64 : ELFCLASS32, ELFDATA2LSB);
    elf_writer.set_type(ET_REL);
    if (arch_ == Arch::AARCH32) {
        elf_writer.set_flags(EF_ARM_EABI_VER5);
    }
    elf_writer.set_os_abi(ELFOSABI_NONE);
    elf_writer.set_machine(GetElfArch(arch_));

    ELFIO::section *str_sec = elf_writer.sections.add(".strtab");
    str_sec->set_type(SHT_STRTAB);
    str_sec->set_addr_align(0x1);

    ELFIO::string_section_accessor str_writer(str_sec);

    static constexpr size_t FIRST_GLOBAL_SYMBOL_INDEX = 2;
    static constexpr size_t SYMTAB_ADDR_ALIGN = 8;

    ELFIO::section *sym_sec = elf_writer.sections.add(".symtab");
    sym_sec->set_type(SHT_SYMTAB);
    sym_sec->set_info(FIRST_GLOBAL_SYMBOL_INDEX);
    sym_sec->set_link(str_sec->get_index());
    sym_sec->set_addr_align(SYMTAB_ADDR_ALIGN);
    sym_sec->set_entry_size(elf_writer.get_default_entry_size(SHT_SYMTAB));

    ELFIO::symbol_section_accessor symbol_writer(elf_writer, sym_sec);

    symbol_writer.add_symbol(str_writer, "irtoc.cpp", 0, 0, STB_LOCAL, STT_FILE, 0, SHN_ABS);

    ELFIO::section *text_sec = elf_writer.sections.add(".text");
    text_sec->set_type(SHT_PROGBITS);
    // NOLINTNEXTLINE(hicpp-signed-bitwise)
    text_sec->set_flags(SHF_ALLOC | SHF_EXECINSTR);
    text_sec->set_addr_align(GetCodeAlignment(arch_));

    ELFIO::section *rel_sec = elf_writer.sections.add(".rela.text");
    rel_sec->set_type(SHT_RELA);
    rel_sec->set_info(text_sec->get_index());
    rel_sec->set_link(sym_sec->get_index());
    rel_sec->set_addr_align(4U);  // CODECHECK-NOLINT(C_RULE_ID_MAGICNUMBER)
    rel_sec->set_entry_size(elf_writer.get_default_entry_size(SHT_RELA));
    ELFIO::relocation_section_accessor rel_writer(elf_writer, rel_sec);

    /* Use symbols map to avoid saving the same symbols multiple times */
    std::unordered_map<std::string, uint32_t> symbols_map;
    auto add_symbol = [&symbols_map, &symbol_writer, &str_writer](const char *name) {
        if (auto it = symbols_map.find(name); it != symbols_map.end()) {
            return it->second;
        }
        uint32_t index = symbol_writer.add_symbol(str_writer, name, 0, 0, STB_GLOBAL, STT_NOTYPE, 0, 0);
        symbols_map.insert({name, index});
        return index;
    };
#ifdef PANDA_COMPILER_DEBUG_INFO
    auto dwarf_builder {has_debug_info_ ? std::make_optional<DwarfBuilder>(arch_, &elf_writer) : std::nullopt};
#endif

    static constexpr size_t MAX_CODE_ALIGNMENT = 64;
    static constexpr std::array<uint8_t, MAX_CODE_ALIGNMENT> PADDING_DATA {0};
    CHECK_LE(GetCodeAlignment(GetArch()), MAX_CODE_ALIGNMENT);

    uint32_t code_alignment = GetCodeAlignment(GetArch());
    size_t offset = 0;
    for (auto unit : units_) {
        auto code = unit->GetCode();
        if (code.empty()) {
            continue;
        }

        // Align function
        if (auto padding = offset % code_alignment; padding != 0) {
            text_sec->append_data(reinterpret_cast<const char *>(PADDING_DATA.data()), padding);
            offset += padding;
        }
        auto symbol = symbol_writer.add_symbol(str_writer, unit->GetName(), offset, code.size(), STB_GLOBAL, STT_FUNC,
                                               0, text_sec->get_index());
        (void)symbol;
        text_sec->append_data(reinterpret_cast<const char *>(code.data()), code.size());
        for (auto &rel : unit->GetRelocations()) {
            size_t rel_offset = offset + rel.offset;
            auto sindex = add_symbol(unit->GetExternalFunction(rel.data));
            if (Is64BitsArch(arch_)) {
                // NOLINTNEXTLINE(hicpp-signed-bitwise)
                rel_writer.add_entry(rel_offset, static_cast<ELFIO::Elf_Xword>(ELF64_R_INFO(sindex, rel.type)),
                                     rel.addend);
            } else {
                // NOLINTNEXTLINE(hicpp-signed-bitwise)
                rel_writer.add_entry(rel_offset, static_cast<ELFIO::Elf_Xword>(ELF32_R_INFO(sindex, rel.type)),
                                     rel.addend);
            }
        }
#ifdef PANDA_COMPILER_DEBUG_INFO
        ASSERT(!unit->GetGraph()->IsLineDebugInfoEnabled() || dwarf_builder);
        if (dwarf_builder && !dwarf_builder->BuildGraph(unit, offset, symbol)) {
            return Unexpected("DwarfBuilder::BuildGraph failed!");
        }
#endif
        offset += code.size();
    }
#ifdef PANDA_COMPILER_DEBUG_INFO
    if (dwarf_builder && !dwarf_builder->Finalize(offset)) {
        return Unexpected("DwarfBuilder::Finalize failed!");
    }
#endif

    elf_writer.save(output.data());

    return 0;
}
}  // namespace panda::irtoc
