# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# There seems to be a bug in either clang-tidy or CMake:
# When clang/gcc is used for cross-compilation, it is ran on host and use defines and options for host
# For example for arm32 cross-compilation Clang-Tidy:
#   - don't know about -march=armv7-a
#   - believes that size of pointer is 64 instead of 32 for aarch32
# TODO: Retry once we upgrade the checker.


if (EXISTS ${PANDA_ROOT}/scripts/extras/clang-tidy/clang_tidy_check.py)
  add_custom_target(clang-tidy-check
      COMMAND ${CMAKE_COMMAND} -E copy ${PANDA_ROOT}/.clang-tidy ${PANDA_BINARY_ROOT}/.clang-tidy
      COMMAND ${PANDA_ROOT}/scripts/extras/clang-tidy/clang_tidy_check.py ${PANDA_ROOT} ${PANDA_BINARY_ROOT}
      USES_TERMINAL
      DEPENDS irtoc_tests panda_bins ark_disasm ark_aotdump dprof_converter dprof_daemon
  )

  add_custom_target(clang-tidy-check-full
      COMMAND ${CMAKE_COMMAND} -E copy ${PANDA_ROOT}/.clang-tidy ${PANDA_BINARY_ROOT}/.clang-tidy
      COMMAND ${PANDA_ROOT}/scripts/extras/clang-tidy/clang_tidy_check.py --full ${PANDA_ROOT} ${PANDA_BINARY_ROOT}
      USES_TERMINAL
      DEPENDS irtoc_tests panda_bins ark_disasm ark_aotdump dprof_converter dprof_daemon
  )
endif()
