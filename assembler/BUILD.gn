# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//ark/runtime_core/ark_config.gni")
import("//build/ohos.gni")

config("arkassembler_public_config") {
  include_dirs = [
    "$ark_root/assembler",
    "$target_gen_dir",
    "$target_gen_dir/include",
    "$root_gen_dir",
    "$root_gen_dir/libpandabase",
    "$ark_root",
  ]

  defines = [ "PANDA_WITH_ECMASCRIPT" ]
}

libarkassembler_sources = [
  "$target_gen_dir/ins_to_string.cpp",
  "annotation.cpp",
  "assembly-emitter.cpp",
  "assembly-ins.cpp",
  "assembly-parser.cpp",
  "assembly-program.cpp",
  "assembly-type.cpp",
  "context.cpp",
  "extensions/extensions.cpp",
  "lexer.cpp",
  "meta.cpp",
]

libarkassembler_configs = [
  sdk_libc_secshared_config,
  "$ark_root:ark_config",
  ":arkassembler_public_config",
  "$ark_root/libpandabase:arkbase_public_config",
  "$ark_root/libpandafile:arkfile_public_config",
  "$ark_root/compiler:arkcompiler_public_config",
]

plugin_deps = []
foreach(plugin, enabled_plugins) {
  print("add assembler plugin: $plugin")
  plugin_dir = "$ark_root/plugins/$plugin"
  libarkassembler_configs += [ "$plugin_dir:assembler" ]
  plugin_deps += [ "$plugin_dir:assembler_deps" ]

  source_files = read_file("$plugin_dir/subproject_sources.gn", "scope")
  if (defined(source_files.srcs_assembler_path)) {
    source_file = "$plugin_dir/${source_files.srcs_assembler_path}"
    src_scope = read_file(source_file, "scope")
    foreach(src, src_scope.srcs) {
      libarkassembler_sources += [ "$plugin_dir/assembler/$src" ]
    }
    src_scope = {
    }
  }
  source_files = []
}

source_set("libarkassembler_static") {
  sources = libarkassembler_sources

  public_configs = libarkassembler_configs

  deps = [
    ":ark_asm_meta_gen_h",
    ":ark_asm_register_extensions_h",
    ":isa_gen_libarkassembler_ins_create_api_h",
    ":isa_gen_libarkassembler_ins_emit_h",
    ":isa_gen_libarkassembler_ins_to_string_cpp",
    ":isa_gen_libarkassembler_isa_h",
    ":isa_gen_libarkassembler_opcode_parsing_h",
    ":isa_gen_libarkassembler_operand_types_print_h",
    "$ark_root/compiler:libarkcompiler",
    "$ark_root/libpandabase:libarkbase",
    "$ark_root/libpandafile:libarkfile",
    "$ark_root/runtime:profiling_gen_profiling_gen_h",
    sdk_libc_secshared_dep,
  ]

  deps += plugin_deps
}

ohos_shared_library("libarkassembler") {
  deps = [ ":libarkassembler_static" ]

  relative_install_dir = "ark"
  output_extension = "so"
  if (is_mingw) {
    output_extension = "dll"
  }
  subsystem_name = "ark"
}

source_set("libarkassembler_frontend_set_static") {
  sources = libarkassembler_sources

  public_configs = libarkassembler_configs

  deps = [
    ":ark_asm_meta_gen_h",
    ":ark_asm_register_extensions_h",
    ":isa_gen_libarkassembler_ins_create_api_h",
    ":isa_gen_libarkassembler_ins_emit_h",
    ":isa_gen_libarkassembler_ins_to_string_cpp",
    ":isa_gen_libarkassembler_isa_h",
    ":isa_gen_libarkassembler_opcode_parsing_h",
    ":isa_gen_libarkassembler_operand_types_print_h",
    "$ark_root/compiler:libarkcompiler_frontend_static",
    "$ark_root/libpandabase:libarkbase_frontend_static",
    "$ark_root/libpandafile:libarkfile_frontend_static",
    sdk_libc_secshared_dep,
  ]

  deps += plugin_deps
}

ohos_static_library("libarkassembler_frontend_static") {
  deps = [ ":libarkassembler_frontend_set_static" ]
}

source_set("ark_asm_static") {
  sources = [ "pandasm.cpp" ]

  include_dirs = [
    "$target_gen_dir",
    "$root_gen_dir/libpandabase",
  ]

  public_configs = [
    sdk_libc_secshared_config,
    ":arkassembler_public_config",
    "$ark_root:ark_config",
    "$ark_root/libpandabase:arkbase_public_config",
    "$ark_root/libpandafile:arkfile_public_config",
    "$ark_root/compiler:arkcompiler_public_config",
    "$ark_root/runtime:arkruntime_public_config",
  ]

  deps = [
    ":libarkassembler_frontend_static",
    "$ark_root/bytecode_optimizer:libarkbytecodeopt_frontend_static",
    "$ark_root/compiler:libarkcompiler_frontend_static",
    "$ark_root/libpandabase:libarkbase_frontend_static",
    "$ark_root/libpandafile:libarkfile_frontend_static",
  ]

  libs = platform_libs
  ldflags = platform_ldflags
}

ohos_executable("ark_asm") {
  deps = [ ":ark_asm_static" ]

  install_enable = true
  subsystem_name = "ark"
}

ark_isa_gen("isa_gen_libarkassembler") {
  template_files = [
    "isa.h.erb",
    "ins_emit.h.erb",
    "ins_to_string.cpp.erb",
    "ins_create_api.h.erb",
    "opcode_parsing.h.erb",
    "operand_types_print.h.erb",
  ]
  sources = "templates"
  destination = "$target_gen_dir"
  requires = [
    "asm_isapi.rb",
    "../libpandafile/pandafile_isapi.rb",
  ]
}

ark_gen_file("ark_asm_meta_gen_h") {
  template_file = "templates/meta_gen.cpp.erb"
  data_file = "metadata.yaml"
  requires = [ "asm_metadata.rb" ]
  output_file = "$target_gen_dir/meta_gen.h"
}

ark_gen_file("ark_asm_register_extensions_h") {
  extra_dependencies = [ "$ark_root:concat_plugins_yamls" ]
  template_file = "extensions/register_extensions.h.erb"
  data_file = "$target_gen_dir/../plugin_options.yaml"
  requires = [ "$ark_root/templates/plugin_options.rb" ]
  output_file = "$target_gen_dir/register_extensions.h"
}
