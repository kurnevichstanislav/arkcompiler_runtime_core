/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "analysis.h"

#include "optimizer/ir/basicblock.h"
#include "compiler_logger.h"
namespace panda::compiler {

class BasicBlock;
bool FindOsrEntryRec(BasicBlock *dominate_bb, BasicBlock *current_bb, Marker mrk)
{
    if (dominate_bb == current_bb) {
        return false;
    }
    if (current_bb->SetMarker(mrk)) {
        return false;
    }
    if (current_bb->IsOsrEntry()) {
        return true;
    }

    for (auto pred : current_bb->GetPredsBlocks()) {
        if (FindOsrEntryRec(dominate_bb, pred, mrk)) {
            return true;
        }
    }
    return false;
}

bool HasOsrEntryBetween(Inst *dominate_inst, Inst *inst)
{
    auto bb = inst->GetBasicBlock();
    auto graph = bb->GetGraph();
    if (!graph->IsOsrMode()) {
        return false;
    }
    auto dominate_bb = dominate_inst->GetBasicBlock();

    auto mrk = graph->NewMarker();

    auto has_osr_entry = FindOsrEntryRec(dominate_bb, bb, mrk);

    graph->EraseMarker(mrk);
    return has_osr_entry;
}

Inst *InstStoredValue(Inst *inst, Inst **second_value)
{
    ASSERT_PRINT(inst->IsStore(), "Attempt to take a stored value on non-store instruction");
    Inst *val = nullptr;
    *second_value = nullptr;
    switch (inst->GetOpcode()) {
        case Opcode::StoreArray:
        case Opcode::StoreObject:
        case Opcode::StoreStatic:
        case Opcode::StoreArrayI:
        case Opcode::Store:
        case Opcode::StoreI:
            // Last input is a stored value
            val = inst->GetInput(inst->GetInputsCount() - 1).GetInst();
            break;
        case Opcode::UnresolvedStoreObject:
            val = inst->GetInput(1).GetInst();
            break;
        case Opcode::UnresolvedStoreStatic:
            val = inst->GetInput(0).GetInst();
            break;
        case Opcode::StoreArrayPair:
        case Opcode::StoreArrayPairI: {
            val = inst->GetInput(inst->GetInputsCount() - 2U).GetInst();
            auto second_inst = inst->GetInput(inst->GetInputsCount() - 1U).GetInst();
            *second_value = inst->GetDataFlowInput(second_inst);
            break;
        }
        // Unhandled store instructions has been met
        default:
            UNREACHABLE();
    }
    return inst->GetDataFlowInput(val);
}

Inst *InstStoredValue(Inst *inst)
{
    Inst *second_value = nullptr;
    Inst *val = InstStoredValue(inst, &second_value);
    ASSERT(second_value == nullptr);
    return val;
}

bool IsSuitableForImplicitNullCheck(const Inst *inst)
{
    auto is_compressed_enabled = inst->GetBasicBlock()->GetGraph()->GetRuntime()->IsCompressedStringsEnabled();
    switch (inst->GetOpcode()) {
        case Opcode::LoadArray:
            return inst->CastToLoadArray()->IsArray() || !is_compressed_enabled;
        case Opcode::LoadArrayI:
            return inst->CastToLoadArrayI()->IsArray() || !is_compressed_enabled;
        case Opcode::LoadObject:
        // case Opcode::UnresolvedLoadObject:
        case Opcode::StoreObject:
        // case Opcode::UnresolvedStoreObject:
        case Opcode::LenArray:
        case Opcode::StoreArray:
        case Opcode::StoreArrayI:
        case Opcode::LoadArrayPair:
        case Opcode::StoreArrayPair:
        case Opcode::LoadArrayPairI:
        case Opcode::StoreArrayPairI:
            // These instructions access nullptr and produce a signal in place
            // Note that CallVirtual is not in the list
            return true;
        default:
            return false;
    }
}

bool IsInstNotNull(const Inst *inst)
{
    // Allocations cannot return null pointer
    if (inst->IsAllocation() || inst->IsNullCheck()) {
        return true;
    }
    auto graph = inst->GetBasicBlock()->GetGraph();
    auto runtime = graph->GetRuntime();
    // The object is not null if the method is virtual and the object is first parameter.
    return !runtime->IsMethodStatic(graph->GetMethod()) && inst->GetOpcode() == Opcode::Parameter &&
           inst->CastToParameter()->GetArgNumber() == 0;
}

static bool FindObjectInSaveState(const Inst *object, Inst *ss)
{
    while (ss != nullptr && object->IsDominate(ss)) {
        auto it = std::find_if(ss->GetInputs().begin(), ss->GetInputs().end(),
                               [object, ss](Input input) { return ss->GetDataFlowInput(input.GetInst()) == object; });
        if (it != ss->GetInputs().end()) {
            return true;
        }
        auto caller = static_cast<SaveStateInst *>(ss)->GetCallerInst();
        if (caller == nullptr) {
            break;
        }
        ss = caller->GetSaveState();
    }
    return false;
}

// Returns true if GC can be triggered at this point
static bool IsSaveStateForGc(Inst *inst)
{
    if (inst->GetOpcode() == Opcode::SafePoint) {
        return true;
    }
    if (inst->GetOpcode() == Opcode::SaveState) {
        for (auto &user : inst->GetUsers()) {
            if (user.GetInst()->IsRuntimeCall()) {
                return true;
            }
        }
    }
    return false;
}

// Returns true if object is alive and can be user's input
bool IsObjectInSaveStates(const Inst *object, Inst *user)
{
    ASSERT(object->IsDominate(user));
    auto graph = object->GetBasicBlock()->GetGraph();
    auto object_visited = graph->NewMarker();
    auto result = CheckObjectRec(object, user, user->GetBasicBlock(), user->GetPrev(), object_visited);
    graph->EraseMarker(object_visited);
    return result;
}

// Checks if object is correctly used in SaveStates between it and user
bool CheckObjectRec(const Inst *object, const Inst *user, const BasicBlock *block, Inst *start_from, Marker visited)
{
    if (start_from != nullptr) {
        auto it = InstSafeIterator<IterationType::ALL, IterationDirection::BACKWARD>(*block, start_from);
        for (; it != block->AllInstsSafeReverse().end(); ++it) {
            auto inst = *it;
            if (inst == nullptr) {
                break;
            }
            if (inst->SetMarker(visited) || inst == object || inst == user) {
                return true;
            }
            if (IsSaveStateForGc(inst) && !FindObjectInSaveState(object, inst)) {
                return false;
            }
        }
    }
    for (auto pred : block->GetPredsBlocks()) {
        // Catch-begin block has edge from try-end block, and all try-blocks should be visited from this edge.
        // `object` can be placed inside try-block - after try-begin, so that visiting try-begin is wrong
        if (block->IsCatchBegin() && pred->IsTryBegin()) {
            continue;
        }
        CheckObjectRec(object, user, pred, pred->GetLastInst(), visited);
    }
    return true;
}

// Checks if input edges of phi_block come from different branches of dominating if_imm instruction
// Returns true if the first input is in true branch, false if it is in false branch, and std::nullopt
// if branches intersect
std::optional<bool> IsIfInverted(BasicBlock *phi_block, IfImmInst *if_imm)
{
    auto if_block = if_imm->GetBasicBlock();
    ASSERT(if_block == phi_block->GetDominator());
    auto true_bb = if_imm->GetEdgeIfInputTrue();
    auto false_bb = if_imm->GetEdgeIfInputFalse();
    auto pred0 = phi_block->GetPredecessor(0);
    auto pred1 = phi_block->GetPredecessor(1);

    // Triangle case: phi block is the first in true branch
    if (true_bb == phi_block && false_bb->GetPredsBlocks().size() == 1) {
        return pred0 != if_block;
    }
    // Triangle case: phi block is the first in false branch
    if (false_bb == phi_block && true_bb->GetPredsBlocks().size() == 1) {
        return pred0 == if_block;
    }
    // If true_bb has more than one predecessor, there can be a path from false_bb
    // to true_bb avoiding if_imm
    if (true_bb->GetPredsBlocks().size() > 1 || false_bb->GetPredsBlocks().size() > 1) {
        return std::nullopt;
    }
    // Every path through first input edge to phi_block comes from true branch
    // Every path through second input edge to phi_block comes from false branch
    if (true_bb->IsDominate(pred0) && false_bb->IsDominate(pred1)) {
        return false;
    }
    // Every path through first input edge to phi_block comes from false branch
    // Every path through second input edge to phi_block comes from true branch
    if (false_bb->IsDominate(pred0) && true_bb->IsDominate(pred1)) {
        return true;
    }
    // True and false branches intersect
    return std::nullopt;
}
ArenaVector<Inst *> *SaveStateBridgesBuilder::SearchForMissingUserInSS(Graph *graph, Inst *source, Inst *target)
{
    ASSERT(graph != nullptr);
    ASSERT(target != nullptr);
    ASSERT(source != nullptr);

    if (bridges_ == nullptr) {
        auto adapter = graph->GetLocalAllocator();
        bridges_ = adapter->New<ArenaVector<Inst *>>(adapter->Adapter());
    } else {
        bridges_->clear();
    }
    auto visited = graph->NewMarker();
    SearchSSOnWay(target->GetBasicBlock(), target, source, visited, bridges_);
    graph->EraseMarker(visited);
    return bridges_;
}

void SaveStateBridgesBuilder::SearchSSOnWay(BasicBlock *block, Inst *start_from, Inst *end_inst, Marker visited,
                                            ArenaVector<Inst *> *bridges)
{
    ASSERT(block != nullptr);
    ASSERT(end_inst != nullptr);
    ASSERT(bridges != nullptr);

    if (start_from != nullptr) {
        auto it = InstSafeIterator<IterationType::ALL, IterationDirection::BACKWARD>(*block, start_from);
        for (; it != block->AllInstsSafeReverse().end(); ++it) {
            auto inst = *it;
            if (inst == nullptr) {
                break;
            }
            COMPILER_LOG(DEBUG, BRIDGES_SS) << " See inst" << *inst;

            if (inst->SetMarker(visited)) {
                return;
            }
            if (IsSSForGc(inst)) {
                COMPILER_LOG(DEBUG, BRIDGES_SS) << "\tSearch in SS";
                SearchInSSAndFillBridge(inst, end_inst, bridges);
            } else if (inst == end_inst) {
                return;
            }
        }
    }
    for (auto pred : block->GetPredsBlocks()) {
        // Catch-begin block has edge from try-end block, and all try-blocks should be visited from this edge.
        // `object` can be placed inside try-block - after try-begin, so that visiting try-begin is wrong
        if (block->IsCatchBegin() && pred->IsTryBegin()) {
            continue;
        }
        SearchSSOnWay(pred, pred->GetLastInst(), end_inst, visited, bridges);
    }
}

void SaveStateBridgesBuilder::SearchInSSAndFillBridge(Inst *inst, Inst *searched_inst, ArenaVector<Inst *> *bridges)
{
    ASSERT(inst != nullptr);
    ASSERT(searched_inst != nullptr);
    ASSERT(bridges != nullptr);
    auto user = std::find_if(inst->GetInputs().begin(), inst->GetInputs().end(), [searched_inst, inst](Input input) {
        return inst->GetDataFlowInput(input.GetInst()) == searched_inst;
    });

    if (user == inst->GetInputs().end()) {
        COMPILER_LOG(DEBUG, BRIDGES_SS) << "\tNot found";
        bridges->push_back(inst);
    }
}

bool SaveStateBridgesBuilder::IsSSForGc(Inst *inst)
{
    return inst->GetOpcode() == Opcode::SafePoint || inst->GetOpcode() == Opcode::SaveState;
}

bool IsOpcodeRequiresBridgeVN(Inst *inst)
{
    if (inst->GetType() == DataType::REFERENCE) {
        switch (inst->GetOpcode()) {
            case Opcode::LoadAndInitClass:
            case Opcode::LoadClass:
            case Opcode::InitClass:
            case Opcode::GetInstanceClass:
            case Opcode::GetGlobalVarAddress:
                // Opcodes above don't need bridges, because GC can't move this objects.
                COMPILER_LOG(DEBUG, VN_OPT) << " Don't need to do bridge for opcode " << *inst;
                return false;
            default:
                return true;
        }
    }
    return false;
}

void SaveStateBridgesBuilder::CreateBridgeInSS(Inst *source, ArenaVector<Inst *> *bridges)
{
    for (Inst *ss : *bridges) {
        auto num_user = ss->AppendInput(source);
        static_cast<SaveStateInst *>(ss)->SetVirtualRegister(num_user, VirtualRegister(VirtualRegister::BRIDGE, false));
    }
}

void SaveStateBridgesBuilder::SearchAndCreateForMissingUserInSS(Graph *graph, Inst *source, Inst *target)
{
    ASSERT(graph != nullptr);
    ASSERT(target != nullptr);
    ASSERT(source != nullptr);

    auto bridges = SearchForMissingUserInSS(graph, source, target);
    if (!bridges->empty()) {
        CreateBridgeInSS(source, bridges);
        COMPILER_LOG(DEBUG, BRIDGES_SS) << " Created bridge(s)";
    }
}

void SaveStateBridgesBuilder::DumpBridges(std::ostream &out, Inst *source, ArenaVector<Inst *> *bridges)
{
    ASSERT(source != nullptr);
    ASSERT(bridges != nullptr);
    out << "Inst id " << source->GetId() << " with type ";
    source->DumpOpcode(&out);
    out << "need bridge in SS id: ";
    for (auto ss : *bridges) {
        out << ss->GetId() << " ";
    }
    out << '\n';
}

bool StoreValueCanBeObject(Inst *inst)
{
    switch (inst->GetOpcode()) {
        case Opcode::CastValueToAnyType: {
            auto type = AnyBaseTypeToDataType(inst->CastToCastValueToAnyType()->GetAnyType());
            return (type == DataType::ANY || type == DataType::REFERENCE);
        }
        case Opcode::Constant:
            return false;
        default:
            return true;
    }
}

}  // namespace panda::compiler
