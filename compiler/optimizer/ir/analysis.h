/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef COMPILER_OPTIMIZER_IR_ANALYSIS_H_
#define COMPILER_OPTIMIZER_IR_ANALYSIS_H_

#include "graph.h"

#include <optional>

namespace panda::compiler {

/**
 * The file contains small analysis functions which can be used in different passes
 */
class Inst;
// returns Store value, for StoreArrayPair and StoreArrayPairI saved not last store value in second_value
Inst *InstStoredValue(Inst *inst, Inst **second_value);
Inst *InstStoredValue(Inst *inst);
bool HasOsrEntryBetween(Inst *dominate_inst, Inst *inst);
bool IsSuitableForImplicitNullCheck(const Inst *inst);
bool IsInstNotNull(const Inst *inst);
bool IsObjectInSaveStates(const Inst *object, Inst *user);
bool CheckObjectRec(const Inst *object, const Inst *user, const BasicBlock *block, Inst *start_from, Marker visited);
std::optional<bool> IsIfInverted(BasicBlock *phi_block, IfImmInst *if_imm);
bool IsOpcodeRequiresBridgeVN(Inst *inst);

/**
 * Functions below are using for create bridge in SaveStates between source instruction and target instruction.
 * It use in GVN etc. It inserts `source` instruction into `SaveStates` on each path between `source` and
 * `target` instructions to save the object in case GC is triggered on this path.
 * Instructions on how to use it: compiler/docs/bridges.md
 */
class SaveStateBridgesBuilder {
public:
    ArenaVector<Inst *> *SearchForMissingUserInSS(Graph *graph, Inst *source, Inst *target);
    void CreateBridgeInSS(Inst *source, ArenaVector<Inst *> *bridges);
    void SearchAndCreateForMissingUserInSS(Graph *graph, Inst *source, Inst *target);
    void DumpBridges(std::ostream &out, Inst *source, ArenaVector<Inst *> *bridges);

private:
    void SearchSSOnWay(BasicBlock *block, Inst *start_from, Inst *end_inst, Marker visited,
                       ArenaVector<Inst *> *bridges);
    bool IsSSForGc(Inst *inst);
    void SearchInSSAndFillBridge(Inst *inst, Inst *searched_inst, ArenaVector<Inst *> *bridges);
    /**
     * Pointer to moved out to class for reduce memory usage in each pair of equal instructions.
     * When using functions, it looks like we work as if every time get a new vector,
     * but one vector is always used and cleaned before use.
     */
    ArenaVector<Inst *> *bridges_ {nullptr};
};
bool StoreValueCanBeObject(Inst *inst);
}  // namespace panda::compiler

#endif  // COMPILER_OPTIMIZER_IR_ANALYSIS_H_
