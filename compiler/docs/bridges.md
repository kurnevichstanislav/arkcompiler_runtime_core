# SaveState Bridges

## Overview
This is a tool that can be used in optimizations that remove duplicate elements. `SSB` receives 2 instructions: `source` and `target`. It inserts `source` instruction into `SaveStates` on each path between `source` and `target` instructions to save the object in case GC is triggered on this path.

## Rationality
Fix the graph after optimizations, for example, after remove duplicate objects.

## Dependence
We need to be sure that the `source` instruction dominates the `target` instruction.

## Algorithm
We bypass the graph in the opposite direction from the `target` instruction to the `source` and we are looking for SS that need to be fixed. We always can do it, because we sure, that the `source` instruction dominates `target`.

## Usage
Create **`SaveStateBridgesBuilder`** in place where during all time of the pass object will be live. For example, in `.h` file of pass. Functions below are class methods.

**`SearchForMissingUserInSS`** is using for search SS, which don't have `source` inst in user. Return `ArenaVector<Inst *> *` all of these SS. If is empty, bridges are not needed.

**`CreateBridgeInSS`** is using for append `source` as a bridge in all SS from `ArenaVector<Inst *> *` received above.

**`SearchAndCreateForMissingUserInSS`** includes `SearchForMissingUserInSS` and, if it need it, `CreateBridgeInSS`.

**`DumpBridges`** write in your `std::ostream` all bridges which need add for this `source` instruction.

## Simple example of work without any optimization

### Before:

Receiving and using is at a distance, and the object is not recorded in the intermediate SS. This is an incorrect graph, because after SS v4 or SS v7 GC can be triggered. So obj `1.ref` can be deleted or moved, but the pointer will not change. As a result, in inst v10 we can use the object at an **invalid address**.
```
    1.ref  Intrinsic.GET        (v10)
    ...
    4.     SaveState            ...
    ...
    7.     SaveState            ...
    ...
    10.    Intrinsic.USE        v1
```

### After `SaveState Bridges`:
Here the tool corrected SS thus restored the object's safety.

```
    1.ref  Intrinsic.GET        (v10)
    ...
    4.     SaveState            v1(bridge), ...
    ...
    7.     SaveState            v1(bridge), ...
    ...
    10.    Intrinsic.USE        v1
```

## Example in VN

Instrinsic below with clear flag `NO_CSE`.

Before VN:

```
    0.ref  Intrinsic.GET        (v1)   <==|
    1.void Intrinsic.USE        v0        |
    2.     SaveState            -> ...    |
    3.ref  Intrinsic.GET        (v4)   <==| Inst v3 is equal inst v0,
    4.void Intrinsic.USE        v3        | they return the same ref
```

After VN with bridges + Cleanup:
```
    0.ref  Intrinsic.GET        (v4, v2, v1)
    1.void Intrinsic.USE        v0
    2.     SaveState            v0(bridge) -> ...  <==| Added bridge on way
                                                      | to target instruction
                                        <==| VN delete inst v3
    4.void Intrinsic.USE        v0
```

## Links

Source code:
[analysis.h](../optimizer/ir/analysis.h)
[analysis.cpp](../optimizer/ir/analysis.cpp)

Tests:
[Tests for GVN](../tests/vn_test.cpp)
