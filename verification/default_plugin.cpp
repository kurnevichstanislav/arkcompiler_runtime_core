/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "default_plugin.h"
#include "abs_int_inl_compat_checks.h"
#include "source_lang_enum.h"
#include "verification/jobs/cache.h"
#include "verification/type/type_type.h"
#include "verifier_messages.h"

namespace panda::verifier::plugin {

// Dummy implementation
// TODO(gogabr): make it more sensible
void DefaultPlugin::TypeSystemSetup([[maybe_unused]] PandaTypes *types) const {}

static void SetArraySubtyping(Type t, PandaTypes *types)
{
    auto type_system = &types->GetTypeSystem();
    PandaVector<Type> to_process;
    PandaVector<Type> just_subtype;
    t.ForAllSupertypes(type_system, [&](Type st) {
        if (!types->Array().ExistsWithTypeArgs({+st}, type_system)) {
            to_process.emplace_back(st);
        } else {
            just_subtype.emplace_back(st);
        }
        return true;
    });
    auto array_type = types->Array().WithTypeArgs({+t}, type_system);
    for (const auto &st : just_subtype) {
        MakeSubtype(array_type, types->Array().WithTypeArgs({+st}, type_system), type_system);
    }
    for (const auto &st : to_process) {
        MakeSubtype(array_type, types->Array().WithTypeArgs({+st}, type_system), type_system);
        SetArraySubtyping(st, types);
    }
}

Type TypeOfArray(CachedClass const *klass, PandaTypes *types)
{
    ASSERT(klass->flags[CachedClass::Flag::ARRAY_CLASS]);

    Type type {};
    auto type_system = &types->GetTypeSystem();
    auto component = klass->GetArrayComponent();
    if (component.HasRef()) {
        auto component_type = types->TypeOf(component.Get());
        type = types->Array().WithTypeArgs({+component_type}, type_system);
        SetArraySubtyping(component_type, types);
    } else {
        type = types->Array().WithTypeArgs({+types->Top()}, type_system);
        LOG_VERIFIER_JAVA_TYPES_ARRAY_COMPONENT_TYPE_IS_UNDEFINED();
    }
    MakeSubtype(type, types->ArrayType(), type_system);
    if (klass->flags[CachedClass::Flag::OBJECT_ARRAY_CLASS]) {
        MakeSubtype(type, types->ObjectArrayType(), type_system);
    }

    return type;
}

Type DefaultPlugin::TypeOfClass(CachedClass const *klass, PandaTypes *types) const
{
    auto type_system = &types->GetTypeSystem();
    PandaVector<Type> supertypes;
    for (const auto &ancestor : klass->ancestors) {
        // ancestor here cannot be unresolved descriptor
        ASSERT(LibCache::IsRef(ancestor));
        supertypes.emplace_back(types->TypeOf(LibCache::GetRef(ancestor)));
    }

    bool is_primitive = klass->flags[CachedClass::Flag::PRIMITIVE];
    auto class_name = klass->GetName();
    Type type {};
    if (klass->flags[CachedClass::Flag::ARRAY_CLASS]) {
        type = TypeOfArray(klass, types);
    } else if (is_primitive) {
        type = types->TypeOf(klass->type_id);
    } else {
        type = types->TypeForName(class_name);
    }

    if (!is_primitive) {
        bool is_string = klass->flags[CachedClass::Flag::STRING_CLASS];
        if (is_string) {
            MakeSubtype(type, types->StringType(), type_system);
        } else {
            MakeSubtype(type, types->ObjectType(), type_system);
        }
        MakeSubtype(types->NullRefType(), type, type_system);
        MakeSubtype(type, types->RefType(), type_system);
        MakeSubtype(types->TypeClass().WithTypeArgs({~type}, type_system), types->TypeClassType(), type_system);
        MakeSubtype(types->TypeClassType(), types->RefType(), type_system);
    }
    if (klass->flags[CachedClass::Flag::ABSTRACT]) {
        MakeSubtype(types->Abstract().WithTypeArgs({~type}, type_system), types->AbstractType(), type_system);
    }
    for (auto &super : supertypes) {
        MakeSubtype(type, super, type_system);
    }

    return type;
}

Type DefaultPlugin::TypeOfMethod(CachedMethod const *method, PandaTypes *types) const
{
    auto type_system = &types->GetTypeSystem();
    auto &&sig = types->MethodSignature(*method);
    Type type = types->Method().WithTypeArgs(sig, type_system);
    MakeSubtype(type, types->MethodType(), type_system);
    return type;
}

Type DefaultPlugin::TypeOfTypeId(panda_file::Type::TypeId id, [[maybe_unused]] PandaTypes *types) const
{
    using TypeId = panda_file::Type::TypeId;
    Type type {};
    switch (id) {
        case TypeId::VOID:
            type = types->Top();
            break;
        case TypeId::U1:
            type = types->U1();
            break;
        case TypeId::I8:
            type = types->I8();
            break;
        case TypeId::U8:
            type = types->U8();
            break;
        case TypeId::I16:
            type = types->I16();
            break;
        case TypeId::U16:
            type = types->U16();
            break;
        case TypeId::I32:
            type = types->I32();
            break;
        case TypeId::U32:
            type = types->U32();
            break;
        case TypeId::I64:
            type = types->I64();
            break;
        case TypeId::U64:
            type = types->U64();
            break;
        case TypeId::F32:
            type = types->F32();
            break;
        case TypeId::F64:
            type = types->F64();
            break;
        case TypeId::REFERENCE:
            type = types->RefType();
            break;
        default:
            LOG_VERIFIER_JAVA_TYPES_CANNOT_CONVERT_TYPE_ID_TO_TYPE(id);
            type = types->Top();
    }
    return type;
}

Type DefaultPlugin::NormalizeType(Type type, [[maybe_unused]] PandaTypes *types) const
{
    auto type_system = &types->GetTypeSystem();
    Type result = type;
    if (IsSubtype(type, types->Integral32Type(), type_system)) {
        result = types->Normalize().WithTypeArgs({~types->Integral32Type()}, type_system);
    } else if (IsSubtype(type, types->Integral64Type(), type_system)) {
        result = types->Normalize().WithTypeArgs({~types->Integral64Type()}, type_system);
        // NOLINTNEXTLINE(bugprone-branch-clone)
    } else if (IsSubtype(type, types->F32(), type_system)) {
        result = types->Normalize().WithTypeArgs({~types->F32()}, type_system);
    } else if (IsSubtype(type, types->F64(), type_system)) {
        result = types->Normalize().WithTypeArgs({~types->F64()}, type_system);
    } else if (IsSubtype(type, types->MethodType(), type_system)) {
        result = types->NormalizedMethod().WithTypeArgs(types->NormalizeMethodSignature(type.GetTypeArgs(type_system)),
                                                        type_system);
    }
    return result;
}

}  // namespace panda::verifier::plugin
