/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "public.h"
#include "public_internal.h"
#include "verification/config/config_load.h"
#include "verification/config/context/context.h"
#include "verification/cache/results_cache.h"
#include "verification/jobs/thread_pool.h"

namespace panda::verifier {

Config *NewConfig()
{
    auto result = new Config;
    result->opts_.Initialize();
    return result;
}

bool LoadConfigFile([[maybe_unused]] Config *config, std::string_view config_file_name)
{
    return panda::verifier::config::LoadConfig(config_file_name);
}

void DestroyConfig(Config *config)
{
    config->opts_.Destroy();
    delete config;
}

bool IsEnabled(Config const *config)
{
    ASSERT(config != nullptr);
    return config->opts_.IsEnabled();
}

bool IsOnlyVerify(Config const *config)
{
    ASSERT(config != nullptr);
    return config->opts_.IsOnlyVerify();
}

bool NeedShowStatus(Config const *config)
{
    ASSERT(config != nullptr);
    return config->opts_.Show.Status;
}

bool ShouldSkip([[maybe_unused]] Config const *config, panda::Method const *method)
{
    ASSERT(config != nullptr);
    return debug::SkipVerification(method->GetUniqId());
}

struct Service {
    Config const *config;
};

Service *CreateService(Config const *config, int num_threads, panda::mem::InternalAllocatorPtr allocator,
                       std::string const &cache_file_name)
{
    ThreadPool::Initialize(allocator, num_threads);
    if (!cache_file_name.empty()) {
        VerificationResultCache::Initialize(cache_file_name);
    }
    auto res = new Service;
    res->config = config;
    return res;
}

void StartService([[maybe_unused]] Service *service)
{
    ThreadPool::Start();
}

void DestroyService(Service *service, bool update_cache_file)
{
    if (service == nullptr) {
        return;
    }
    ThreadPool::Destroy();
    VerificationResultCache::Destroy(update_cache_file);
    delete service;
}

Config const *GetServiceConfig(Service *service)
{
    return service->config;
}

void CacheLibraryFiles([[maybe_unused]] Service *service, PandaVector<panda_file::File const *> const &files)
{
    ASSERT(service != nullptr);
    verifier::ThreadPool::GetCache()->FastAPI().ProcessFiles(files);
}

bool Submit([[maybe_unused]] Service *service, panda::Method *method)
{
    ASSERT(service != nullptr);
    bool res = ThreadPool::Enqueue(method);
    if (res) {
        LOG(DEBUG, VERIFIER) << "Method '" << method->GetFullName() << "' enqueued for verification";
    } else {
        LOG(DEBUG, VERIFIER) << "Method '" << method->GetFullName() << "' cannot be enqueued for verification";
    }
    return res;
}

void Wait([[maybe_unused]] Service *service, panda::Method *method, WaitHandler &&need_to_wait_more,
          FailureHandler &&handle_failure)
{
    ASSERT(service != nullptr);
    ThreadPool::WaitForVerification([&] { return need_to_wait_more(method); }, [&] { handle_failure(method); });
}

void SetVerified([[maybe_unused]] Service *service, panda::Method *method, bool result)
{
    ASSERT(service != nullptr);
    VerificationResultCache::CacheResult(method->GetUniqId(), result);
    ThreadPool::SignalMethodVerified();
}

// Do we really need thos public/private distinction here?
inline Status ToPublic(VerificationResultCache::Status status)
{
    switch (status) {
        case VerificationResultCache::Status::OK:
            return Status::OK;
        case VerificationResultCache::Status::FAILED:
            return Status::FAILED;
        case VerificationResultCache::Status::UNKNOWN:
            return Status::UNKNOWN;
        default:
            UNREACHABLE();
    }
}

Status GetResult([[maybe_unused]] Service *service, panda::Method *method)
{
    ASSERT(service != nullptr);
    if (!VerificationResultCache::Enabled()) {
        return Status::UNKNOWN;
    }
    return ToPublic(VerificationResultCache::Check(method->GetUniqId()));
}

void AddToDebugContext(panda::Method const *method, bool is_debug)
{
    debug::DebugContext::GetCurrent().AddMethod(*method, is_debug);
}

void DestroyDebugContext()
{
    debug::DebugContext::Destroy();
}

}  // namespace panda::verifier
