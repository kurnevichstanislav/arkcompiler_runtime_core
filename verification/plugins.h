/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_VERIFICATION_PLUGINS_H__
#define PANDA_VERIFICATION_PLUGINS_H__

#include "abs_int_inl_compat_checks.h"
#include "source_lang_enum.h"
#include "verification/jobs/cache.h"
#include "verification/type/type_type.h"

namespace panda::verifier::plugin {

class Plugin {
public:
    virtual void TypeSystemSetup(PandaTypes *types) const = 0;

    virtual CheckResult const &CheckFieldAccessViolation(CachedField const *field, CachedMethod const *from,
                                                         PandaTypes *types) const = 0;

    virtual CheckResult const &CheckMethodAccessViolation(CachedMethod const *method, CachedMethod const *from,
                                                          PandaTypes *types) const = 0;

    virtual CheckResult const &CheckClassAccessViolation(CachedClass const *klass, CachedMethod const *from,
                                                         PandaTypes *types) const = 0;

    virtual Type TypeOfClass(CachedClass const *klass, PandaTypes *types) const = 0;
    virtual Type TypeOfMethod(CachedMethod const *method, PandaTypes *types) const = 0;
    virtual Type TypeOfTypeId(panda_file::Type::TypeId id, PandaTypes *types) const = 0;

    virtual Type NormalizeType(Type type, PandaTypes *types) const = 0;
};

Plugin const *GetLanguagePlugin(panda_file::SourceLang lang);

}  // namespace panda::verifier::plugin

#endif
