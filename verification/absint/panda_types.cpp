/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "panda_types.h"

#include "runtime/include/class_linker.h"
#include "runtime/include/method.h"
#include "runtime/include/method-inl.h"
#include "runtime/include/class.h"
#include "runtime/include/runtime.h"

#include "utils/span.h"
#include "verification/type/type_arg.h"
#include "verification/type/type_image.h"
#include "verification/type/type_sort.h"
#include "verification/type/type_system.h"
#include "verification/type/type_type.h"
#include "verification/jobs/cache.h"
#include "verification/plugins.h"

#include "verifier_messages.h"

#include "utils/logger.h"

#include "runtime/include/mem/panda_containers.h"
#include "runtime/include/mem/panda_string.h"

#include "libpandabase/os/mutex.h"

namespace panda::verifier {

Type PandaTypes::NormalizedTypeOf(Type type)
{
    ASSERT(type.IsValid());
    if (type_system_.IsBot(type) || type_system_.IsTop(type)) {
        return type;
    }
    auto t = NormalizedTypeOf_.find(type);
    if (t != NormalizedTypeOf_.cend()) {
        return t->second;
    }
    Type result = plugin_->NormalizeType(type, this);
    NormalizedTypeOf_[type] = result;
    return result;
}

TypeArgs PandaTypes::NormalizeMethodSignature(const TypeArgs &sig)
{
    TypeArgs result;
    for (auto const &type_arg : sig) {
        const Type &type = Type(TypeArg {type_arg});
        result.push_back(NormalizedTypeOf(type) * type_arg.Variance());
    };
    return result;
}

const TypeArgs &PandaTypes::NormalizedMethodSignature(const CachedMethod &method)
{
    auto &&method_id = method.id;
    auto it = NormalizedSigOfMethod_.find(method_id);
    if (it != NormalizedSigOfMethod_.end()) {
        return it->second;
    }
    auto &&sig = MethodSignature(method);
    auto &&normalized_sig = NormalizeMethodSignature(sig);
    NormalizedSigOfMethod_[method_id] = normalized_sig;
    return NormalizedSigOfMethod_[method_id];
}

const TypeArgs &PandaTypes::MethodSignature(const CachedMethod &method)
{
    auto &&method_id = method.id;
    auto it = SigOfMethod_.find(method_id);
    if (it != SigOfMethod_.end()) {
        return it->second;
    }
    TypeArgs type_args;
    Type return_type {};
    bool return_is_processed = false;
    for (const auto &arg : method.signature) {
        Type t = LibCache::Visit(
            arg,
            [this](const LibCache::CachedClass &cached_class) {
                return cached_class.type_id == TypeId::VOID ? Top() : TypeOf(cached_class);
            },
            [&]([[maybe_unused]] const LibCache::DescriptorString &descriptor) {
                LOG_VERIFIER_JAVA_TYPES_METHOD_ARG_WAS_NOT_RESOLVED(method.GetName());
                return Top();
            });

        if (!t.IsValid()) {
            LOG_VERIFIER_JAVA_TYPES_METHOD_ARG_CANNOT_BE_CONVERTED_TO_TYPE(method.GetName());
        }
        if (return_is_processed) {
            type_args.push_back(-t);
        } else {
            return_type = t;
            return_is_processed = true;
        }
    }
    type_args.push_back(+return_type);
    SigOfMethod_[method_id] = type_args;
    return SigOfMethod_[method_id];
}

PandaTypes::TypeId PandaTypes::TypeIdOf(const Type &type) const
{
    if (type == U1()) {
        return TypeId::U1;
    }
    if (type == U8()) {
        return TypeId::U8;
    }
    if (type == U16()) {
        return TypeId::U16;
    }
    if (type == U32()) {
        return TypeId::U32;
    }
    if (type == U64()) {
        return TypeId::U64;
    }
    if (type == I8()) {
        return TypeId::I8;
    }
    if (type == I16()) {
        return TypeId::I16;
    }
    if (type == I32()) {
        return TypeId::I32;
    }
    if (type == I64()) {
        return TypeId::I64;
    }
    if (type == F32()) {
        return TypeId::F32;
    }
    if (type == F64()) {
        return TypeId::F64;
    }
    if (type_system_.IsTop(type)) {
        return TypeId::VOID;
    }

    return TypeId::INVALID;
}

Type PandaTypes::TypeOf(const CachedMethod &method)
{
    auto id = method.id;
    auto k = TypeOfMethod_.find(id);
    if (k != TypeOfMethod_.end()) {
        return k->second;
    }
    ASSERT(!DoNotCalculateMethodType_);
    Type result = plugin_->TypeOfMethod(&method, this);
    TypeOfMethod_[id] = result;
    // Normalize(Method) <: NormalizedMethod(NormalizedMethodSig)
    NormalizedTypeOf_[result] = plugin_->NormalizeType(result, this);
    MethodNameOfId_[id] = method.GetName();
    return result;
}

Type PandaTypes::TypeOf(const CachedClass &klass)
{
    auto id = klass.id;
    auto k = TypeOfClass_.find(id);
    if (k != TypeOfClass_.end()) {
        return k->second;
    }

    Type type = plugin_->TypeOfClass(&klass, this);

    auto class_name = klass.GetName();

    ClassNameOfId_[id] = class_name;
    TypeOfClass_[id] = type;
    NormalizedTypeOf(type);
    return type;
}

Type PandaTypes::TypeOf(PandaTypes::TypeId id)
{
    return plugin_->TypeOfTypeId(id, this);
}

PandaTypes::PandaTypes(panda_file::SourceLang lang)
    : plugin_ {plugin::GetLanguagePlugin(lang)},
      sort_names_ {"Bot", "Top"},
      type_system_ {sort_names_["Bot"], sort_names_["Top"]},
      type_image_ {&sort_names_, &type_system_},
      Array_ {ParametricTypeForName("Array")},
      Method_ {ParametricTypeForName("Method")},
      NormalizedMethod_ {ParametricTypeForName("NormalizedMethod")},
      Normalize_ {ParametricTypeForName("Normalize")},
      Abstract_ {ParametricTypeForName("Abstract")},
      Interface_ {ParametricTypeForName("Interface")},
      TypeClass_ {ParametricTypeForName("TypeClass")},

      U1_ {TypeForName("u1")},
      I8_ {TypeForName("i8")},
      U8_ {TypeForName("u8")},
      I16_ {TypeForName("i16")},
      U16_ {TypeForName("u16")},
      I32_ {TypeForName("i32")},
      U32_ {TypeForName("u32")},
      I64_ {TypeForName("i64")},
      U64_ {TypeForName("u64")},
      F32_ {TypeForName("f32")},
      F64_ {TypeForName("f64")},

      RefType_ {TypeForName("RefType")},
      ObjectType_ {TypeForName("ObjectType")},
      StringType_ {TypeForName("StringType")},
      PrimitiveType_ {TypeForName("PrimitiveType")},
      AbstractType_ {TypeForName("AbstractType")},
      InterfaceType_ {TypeForName("InterfaceType")},
      TypeClassType_ {TypeForName("TypeClassType")},
      InstantiableType_ {TypeForName("InstantiableType")},
      ArrayType_ {TypeForName("ArrayType")},
      ObjectArrayType_ {TypeForName("ObjectArrayType")},
      MethodType_ {TypeForName("MethodType")},
      StaticMethodType_ {TypeForName("StaticMethodType")},
      NonStaticMethodType_ {TypeForName("NonStaticMethodType")},
      VirtualMethodType_ {TypeForName("VirtualMethodType")},
      NullRefType_ {TypeForName("NullRefType")},
      Bits32Type_ {TypeForName("32Bits")},
      Bits64Type_ {TypeForName("64Bits")},
      Integral8Type_ {TypeForName("Integral8Bits")},
      Integral16Type_ {TypeForName("Integral16Bits")},
      Integral32Type_ {TypeForName("Integral32Bits")},
      Integral64Type_ {TypeForName("Integral64Bits")},
      Float32Type_ {TypeForName("Float32Bits")},
      Float64Type_ {TypeForName("Float64Bits")}
{
    LanguageContextBase *ctx = panda::plugins::GetLanguageContextBase(lang);

    auto emplace = [&](auto &types, const char *name) {
        if (name != nullptr) {
            types.emplace(lang, TypeForName(name));
        }
    };

    emplace(LangContextTypesClass_, ctx->GetVerificationTypeClass());
    emplace(LangContextTypesObjects_, ctx->GetVerificationTypeObject());
    emplace(LangContextTypesThrowables_, ctx->GetVerificationTypeThrowable());

    type_system_.SetIncrementalRelationClosureMode(false);

    // base subtyping of primitive types
    MakeSubtype(I8(), I16(), &type_system_);
    MakeSubtype(I16(), I32(), &type_system_);
    MakeSubtype(U1(), U8(), &type_system_);
    MakeSubtype(U8(), U16(), &type_system_);
    MakeSubtype(U16(), U32(), &type_system_);

    // integral
    for (auto ii : {U1(), I8(), U8()}) {
        MakeSubtype(ii, Integral8Type(), &type_system_);
    }
    for (auto ii : {Integral8Type(), I16(), U16()}) {
        MakeSubtype(ii, Integral16Type(), &type_system_);
    }
    for (auto ii : {Integral16Type(), I32(), U32()}) {
        MakeSubtype(ii, Integral32Type(), &type_system_);
    }
    for (auto ii : {I64(), U64()}) {
        MakeSubtype(ii, Integral64Type(), &type_system_);
    }

    // sizes
    MakeSubtype(F32(), Float32Type(), &type_system_);
    MakeSubtype(F64(), Float64Type(), &type_system_);
    for (auto tt : {Integral32Type(), Float32Type()}) {
        MakeSubtype(tt, Bits32Type(), &type_system_);
    }
    for (auto tt : {Integral64Type(), Float64Type()}) {
        MakeSubtype(tt, Bits64Type(), &type_system_);
    }
    for (auto tt : {Bits32Type(), Bits64Type()}) {
        MakeSubtype(tt, PrimitiveType(), &type_system_);
    }

    MakeSubtype(TypeClassType(), RefType(), &type_system_);

    auto object_or_ref = {ObjectType(), RefType()};
    if (LangContextTypesClass_.count(lang) > 0) {
        auto types = LangContextTypesClass_.at(lang);
        MakeSubtype(NullRefType(), types, &type_system_);
        for (auto oo : object_or_ref) {
            MakeSubtype(types, oo, &type_system_);
        }
    }
    if (LangContextTypesObjects_.count(lang) > 0) {
        auto objects = LangContextTypesObjects_.at(lang);
        MakeSubtype(NullRefType(), objects, &type_system_);
        for (auto oo : object_or_ref) {
            MakeSubtype(objects, oo, &type_system_);
        }
    }
    if (LangContextTypesThrowables_.count(lang) > 0) {
        auto throwables = LangContextTypesThrowables_.at(lang);
        MakeSubtype(NullRefType(), throwables, &type_system_);
        for (auto oo : object_or_ref) {
            MakeSubtype(throwables, oo, &type_system_);
        }
    }
    for (auto ao : {ArrayType(), ObjectArrayType()}) {
        MakeSubtype(NullRefType(), ao, &type_system_);
        MakeSubtype(ao, RefType(), &type_system_);
    }

    type_system_.CloseSubtypingRelation();

    type_system_.SetIncrementalRelationClosureMode(false);
    type_system_.SetDeferIncrementalRelationClosure(false);

    plugin_->TypeSystemSetup(this);
}

}  // namespace panda::verifier
