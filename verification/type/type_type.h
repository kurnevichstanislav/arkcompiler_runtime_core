/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _PANDA_TYPE_TYPE_HPP__
#define _PANDA_TYPE_TYPE_HPP__

#include "type_sort.h"
#include "type_arg.h"
#include "type_info.h"
#include "type_tags.h"

namespace panda::verifier {
class TypeSystem;
class TypeSet;
class TypeArg;

class Type {
public:
    Type(TypeNum num) : Idx_ {num} {}
    Type() = default;
    Type(const Type &) = default;
    Type(Type &&) = default;
    Type &operator=(const Type &) = default;
    Type &operator=(Type &&) = default;
    ~Type() = default;

    bool operator==(const Type &t) const;

    bool operator!=(const Type &t) const;

    // a workaround for absence of mutualy-recursive classes in C++
    template <typename...>
    TypeArg operator+() const;

    template <typename...>
    TypeArg operator-() const;

    template <typename...>
    TypeArg operator~() const;

    SortIdx Sort(TypeSystem const *tsys) const;

    size_t Arity(TypeSystem const *tsys) const;

    template <typename...>
    TypeArgs GetTypeArgs(TypeSystem const *tsys) const;

    size_t TypeArgsSize(TypeSystem const *tsys) const;

    bool IsValid() const;

    template <typename...>
    TypeArg operator*(TypeVariance variance) const;

    TypeSet AllSupertypes(TypeSystem const *tsys);

    // TODO(vdyadov): implement

    template <typename Handler>
    void ForAllTypeArgs(Handler &&handler) const;

    template <typename Handler>
    void ForAllSupertypes(TypeSystem const *tsys, Handler &&handler) const;

    template <typename Handler>
    void ForAllSupertypesOfSort(TypeSystem const *tsys, SortIdx sort, Handler &&handler) const;

    template <typename Handler>
    void ForAllSubtypes(TypeSystem const *tsys, Handler &&handler) const;

    template <typename Handler>
    void ForAllSubtypesOfSort(TypeSystem const *tsys, SortIdx sort, Handler &&handler) const;

private:
    TypeNum Number() const;

    TypeNum Idx_;

    friend class TypeSystem;
    friend class TypeArg;
    friend class ParametricType;
    friend class PandaTypes;
    friend class TypeSet;

    friend class TypeImage;

    friend struct std::hash<Type>;

    friend bool IsSubtype(Type, Type, TypeSystem const *);
    friend void MakeSubtype(Type, Type, TypeSystem *);
};

bool IsSubtype(Type lhs, Type rhs, TypeSystem const *tsys);
void MakeSubtype(Type sub, Type super, TypeSystem *tsys);

}  // namespace panda::verifier

namespace std {
template <>
struct hash<panda::verifier::Type> {
    size_t operator()(const panda::verifier::Type &type) const
    {
        return static_cast<size_t>(type.Number());
    }
};
}  // namespace std

#endif  // !_PANDA_TYPE_TYPE_HPP__
