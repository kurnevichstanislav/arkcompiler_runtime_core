/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "util/tests/environment.h"

#include "util/tests/verifier_test.h"

#include "type/type_system.h"
#include "type/type_sort.h"
#include "type/type_image.h"

#include "include/runtime.h"

#include "runtime/include/mem/panda_string.h"

#include <gtest/gtest.h>

namespace panda::verifier::test {

TEST_F(VerifierTest, TypeSystemIncrementalClosure)
{
    SortNames sort_names {"Bot", "Top"};
    TypeSystem type_system {sort_names["Bot"], sort_names["Top"]};
    auto paramType = [&type_system, &sort_names](const auto &name) { return type_system.Parametric(sort_names[name]); };

    type_system.SetIncrementalRelationClosureMode(true);
    type_system.SetDeferIncrementalRelationClosure(false);

    auto bot = type_system.Bot();
    auto top = type_system.Top();

    auto i8 = paramType("i8").WithTypeArgs({}, &type_system);
    auto i16 = paramType("i16").WithTypeArgs({}, &type_system);
    auto i32 = paramType("i32").WithTypeArgs({}, &type_system);
    auto i64 = paramType("i64").WithTypeArgs({}, &type_system);

    auto u8 = paramType("u8").WithTypeArgs({}, &type_system);
    auto u16 = paramType("u16").WithTypeArgs({}, &type_system);
    auto u32 = paramType("u32").WithTypeArgs({}, &type_system);
    auto u64 = paramType("u64").WithTypeArgs({}, &type_system);

    auto method = paramType("method");

    auto top_method_of3args = method.WithTypeArgs({-bot, -bot, +top}, &type_system);
    auto bot_method_of3args = method.WithTypeArgs({-top, -top, +bot}, &type_system);

    auto method1 = method.WithTypeArgs({-i8, -i8, +i64}, &type_system);
    auto method2 = method.WithTypeArgs({-i32, -i16, +i32}, &type_system);

    // method2 <: method1
    auto method3 = method.WithTypeArgs({-i16, -method2, +method1}, &type_system);
    auto method4 = method.WithTypeArgs({-i64, -method1, +method2}, &type_system);
    // method4 <: method3

    EXPECT_TRUE(IsSubtype(bot, i8, &type_system));
    EXPECT_TRUE(IsSubtype(bot, u64, &type_system));

    EXPECT_TRUE(IsSubtype(i8, top, &type_system));
    EXPECT_TRUE(IsSubtype(u64, top, &type_system));

    for (auto ii : {i16, i32}) {
        MakeSubtype(i8, ii, &type_system);
        MakeSubtype(ii, i64, &type_system);
    }
    for (auto ul : {u8, u16}) {
        for (auto uh : {u32, u64}) {
            MakeSubtype(ul, uh, &type_system);
        }
    }

    EXPECT_TRUE(IsSubtype(i8, i64, &type_system));
    EXPECT_TRUE(IsSubtype(i16, i64, &type_system));
    EXPECT_TRUE(IsSubtype(i32, i64, &type_system));
    EXPECT_FALSE(IsSubtype(i16, i32, &type_system));

    EXPECT_TRUE(IsSubtype(u8, u64, &type_system));
    EXPECT_TRUE(IsSubtype(u16, u64, &type_system));
    EXPECT_FALSE(IsSubtype(u8, u16, &type_system));
    EXPECT_FALSE(IsSubtype(u32, u64, &type_system));

    EXPECT_TRUE(IsSubtype(method2, method1, &type_system));
    EXPECT_FALSE(IsSubtype(method1, method2, &type_system));

    EXPECT_TRUE(IsSubtype(method4, method3, &type_system));
    EXPECT_FALSE(IsSubtype(method3, method4, &type_system));

    EXPECT_TRUE(IsSubtype(bot_method_of3args, method1, &type_system));
    EXPECT_TRUE(IsSubtype(bot_method_of3args, method4, &type_system));

    EXPECT_TRUE(IsSubtype(method1, top_method_of3args, &type_system));
    EXPECT_TRUE(IsSubtype(method4, top_method_of3args, &type_system));
}

TEST_F(VerifierTest, TypeSystemClosureAtTheEnd)
{
    SortNames sort_names {"Bot", "Top"};
    TypeSystem type_system {sort_names["Bot"], sort_names["Top"]};
    auto paramType = [&type_system, &sort_names](const auto &name) { return type_system.Parametric(sort_names[name]); };

    type_system.SetIncrementalRelationClosureMode(false);
    type_system.SetDeferIncrementalRelationClosure(false);

    auto bot = type_system.Bot();
    auto top = type_system.Top();

    auto i8 = paramType("i8").WithTypeArgs({}, &type_system);
    auto i16 = paramType("i16").WithTypeArgs({}, &type_system);
    auto i32 = paramType("i32").WithTypeArgs({}, &type_system);
    auto i64 = paramType("i64").WithTypeArgs({}, &type_system);

    auto u8 = paramType("u8").WithTypeArgs({}, &type_system);
    auto u16 = paramType("u16").WithTypeArgs({}, &type_system);
    auto u32 = paramType("u32").WithTypeArgs({}, &type_system);
    auto u64 = paramType("u64").WithTypeArgs({}, &type_system);

    auto method = paramType("method");

    auto top_method_of3args = method.WithTypeArgs({-bot, -bot, +top}, &type_system);
    auto bot_method_of3args = method.WithTypeArgs({-top, -top, +bot}, &type_system);

    auto method1 = method.WithTypeArgs({-i8, -i8, +i64}, &type_system);
    auto method2 = method.WithTypeArgs({-i32, -i16, +i32}, &type_system);

    // method2 <: method1
    auto method3 = method.WithTypeArgs({-i16, -method2, +method1}, &type_system);
    auto method4 = method.WithTypeArgs({-i64, -method1, +method2}, &type_system);
    // method4 <: method3

    for (auto ii : {i16, i32}) {
        MakeSubtype(i8, ii, &type_system);
        MakeSubtype(ii, i64, &type_system);
    }
    for (auto ul : {u8, u16}) {
        for (auto uh : {u32, u64}) {
            MakeSubtype(ul, uh, &type_system);
        }
    }

    // before closure all methods are unrelated
    EXPECT_FALSE(IsSubtype(method2, method1, &type_system));
    EXPECT_FALSE(IsSubtype(method1, method2, &type_system));

    EXPECT_FALSE(IsSubtype(method4, method3, &type_system));
    EXPECT_FALSE(IsSubtype(method3, method4, &type_system));

    EXPECT_FALSE(IsSubtype(bot_method_of3args, method1, &type_system));
    EXPECT_FALSE(IsSubtype(bot_method_of3args, method4, &type_system));

    EXPECT_FALSE(IsSubtype(method1, top_method_of3args, &type_system));
    EXPECT_FALSE(IsSubtype(method4, top_method_of3args, &type_system));

    type_system.CloseSubtypingRelation();

    // after closure all relations hold
    EXPECT_TRUE(IsSubtype(method2, method1, &type_system));

    EXPECT_TRUE(IsSubtype(method4, method3, &type_system));
    EXPECT_TRUE(IsSubtype(bot_method_of3args, method1, &type_system));
    EXPECT_TRUE(IsSubtype(method4, top_method_of3args, &type_system));
}

TEST_F(VerifierTest, TypeSystemLeastUpperBound)
{
    SortNames sort_names {"Bot", "Top"};
    TypeSystem type_system {sort_names["Bot"], sort_names["Top"]};
    auto paramType = [&type_system, &sort_names](const auto &name) { return type_system.Parametric(sort_names[name]); };

    /*
        G<--
        ^   \
        |    \
        |     \
        |      E<-   .F
        |      ^  \ /  ^
        D      |   X   |
        ^      |  / \  |
        |      | /   \ |
        |      |/     \|
        A      B       C

        NB!!!
        Here is contradiction to conjecture in relation.h about LUB EqClass.
        So here is many object in LUB class but they are not from the same
        class of equivalence.

        In current Panda type system design with Top and Bot, this issue is not
        significant, because in case of such situation (as with E and F),
        LUB will be Top.

        But in general case assumptions that all elements in LUB are from the same
        class of equivalence is wrong. And corresponding functions in relation.h
        should always return full LUB set. And they should be renamed accordingly,
        to do not mislead other developers.
    */

    auto top = type_system.Top();

    auto a = paramType("A").WithTypeArgs({}, &type_system);
    auto b = paramType("B").WithTypeArgs({}, &type_system);
    auto c = paramType("C").WithTypeArgs({}, &type_system);
    auto d = paramType("D").WithTypeArgs({}, &type_system);
    auto e = paramType("E").WithTypeArgs({}, &type_system);
    auto f = paramType("F").WithTypeArgs({}, &type_system);
    auto g = paramType("G").WithTypeArgs({}, &type_system);

    MakeSubtype(a, d, &type_system);
    MakeSubtype(d, g, &type_system);

    MakeSubtype(b, e, &type_system);
    MakeSubtype(e, g, &type_system);

    MakeSubtype(b, f, &type_system);
    MakeSubtype(c, e, &type_system);
    MakeSubtype(c, f, &type_system);

    auto a_supers = a.AllSupertypes(&type_system);
    auto b_supers = b.AllSupertypes(&type_system);
    auto c_supers = c.AllSupertypes(&type_system);
    auto d_supers = d.AllSupertypes(&type_system);
    auto e_supers = e.AllSupertypes(&type_system);
    auto f_supers = f.AllSupertypes(&type_system);
    auto g_supers = g.AllSupertypes(&type_system);

    auto r = TsIntersection(&a_supers, &b_supers, &type_system);
    EXPECT_EQ(r, (TypeSet {g, top}));

    r = TsIntersection(&e_supers, &f_supers, &type_system);
    EXPECT_EQ(r, TypeSet {top});

    r = TsIntersection(&c_supers, &d_supers, &type_system);
    EXPECT_EQ(r, (TypeSet {g, top}));

    {
        auto ab_supers = TsIntersection(&a_supers, &b_supers, &type_system);
        r = TsIntersection(&ab_supers, &c_supers, &type_system);
    }
    EXPECT_EQ(r, (TypeSet {g, top}));

    {
        auto ab_supers = TsIntersection(&a_supers, &b_supers, &type_system);
        auto abc_supers = TsIntersection(&ab_supers, &c_supers, &type_system);
        r = TsIntersection(&abc_supers, &f_supers, &type_system);
    }
    EXPECT_EQ(r, TypeSet {top});

    EXPECT_TRUE(type_system.IsTop(r.TheOnlyType()));
}

}  // namespace panda::verifier::test
