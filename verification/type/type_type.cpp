/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "type_type.h"
#include "type_arg.h"
#include "type_system.h"
#include "type_sort.h"
#include "type_info.h"
#include "type_set.h"

namespace panda::verifier {

template <>
TypeArg Type::operator+() const
{
    return {TypeVariance::COVARIANT, Idx_};
}

template <>
TypeArg Type::operator-() const
{
    return {TypeVariance::CONTRAVARIANT, Idx_};
}

template <>
TypeArg Type::operator~() const
{
    return {TypeVariance::INVARIANT, Idx_};
}

template <>
TypeArgs Type::GetTypeArgs(TypeSystem const *tsys) const
{
    return TypeArgs {tsys->GetTypeArgs(Idx_)};
}

template <>
TypeArg Type::operator*(TypeVariance variance) const
{
    return {variance, Idx_};
}

bool Type::operator==(const Type &t) const
{
    return t.Idx_ == Idx_;
}

bool Type::operator!=(const Type &t) const
{
    return t.Idx_ != Idx_;
}

bool IsSubtype(Type lhs, Type rhs, TypeSystem const *tsys)
{
    return tsys->IsInDirectRelation(lhs.Idx_, rhs.Idx_);
}

void MakeSubtype(Type sub, Type super, TypeSystem *tsys)
{
    tsys->Relate(sub.Idx_, super.Idx_);
}

SortIdx Type::Sort(TypeSystem const *tsys) const
{
    return tsys->GetSort(Idx_);
}

size_t Type::Arity(TypeSystem const *tsys) const
{
    return tsys->GetArity(Idx_);
}

size_t Type::TypeArgsSize(TypeSystem const *tsys) const
{
    return tsys->GetTypeArgs(Idx_).size();
}

bool Type::IsValid() const
{
    return Idx_ != 0;
}

TypeSet Type::AllSupertypes(TypeSystem const *tsys)
{
    return TypeSet {IntSet {tsys->GetDirectlyRelated(Idx_)}};
}

TypeNum Type::Number() const
{
    return Idx_;
}

}  // namespace panda::verifier
