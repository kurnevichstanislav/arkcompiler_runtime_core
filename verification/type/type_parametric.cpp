/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "type_parametric.h"
#include "type_system.h"

namespace panda::verifier {

bool ParametricType::ExistsWithTypeArgs(TypeArgs type_args, TypeSystem const *tsys) const
{
    Index<TypeNum> num = tsys->FindNum({Sort_, std::move(type_args)});
    return num.IsValid();
}

Type ParametricType::WithTypeArgs(TypeArgs type_args, TypeSystem *tsys) const
{
    auto num = tsys->FindNumOrCreate({Sort_, std::move(type_args)});
    tsys->Relate(tsys->BotNum_, num);
    tsys->Relate(num, tsys->TopNum_);
    return {num};
}

template <typename Handler>
void ParametricType::ForAll(TypeSystem *tsys, Handler &&handler) const
{
    tsys->ForAllTypes([this, tsys, &handler](const Type &type) {
        if (type.Sort(tsys) == Sort_) {
            return handler(type);
        }
        return true;
    });
}

}  // namespace panda::verifier
