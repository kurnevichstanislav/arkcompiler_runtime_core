/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _PANDA_TYPE_IMAGE_HPP
#define _PANDA_TYPE_IMAGE_HPP

#include "type_type.h"

#include "runtime/include/mem/panda_containers.h"
#include "runtime/include/mem/panda_string.h"

namespace panda::verifier {
class TypeImage {
public:
    TypeImage(SortNames const *names, TypeSystem const *tsys) : sort_names_ {names}, type_system_ {tsys} {}
    ~TypeImage() = default;

    PandaString ImageOfVariance(TypeVariance var) const
    {
        switch (var) {
            case TypeVariance::COVARIANT:
                return "+";
            case TypeVariance::CONTRAVARIANT:
                return "-";
            case TypeVariance::INVARIANT:
                return "~";
            default:
                break;
        }
        return "?";
    }

    PandaString ImageOfTypeArg(const TypeArg &type_arg)
    {
        return ImageOfVariance(type_arg.Variance()) + ImageOfType(type_arg.GetType());
    }

    PandaString ImageOfTypeArgs(const TypeArgs &type_args)
    {
        PandaString args_image {""};

        if (type_args.size() != 0) {
            for (const auto &p : type_args) {
                args_image += PandaString {args_image.empty() ? "( " : ", "};
                args_image += ImageOfTypeArg(p);
            };
            args_image += " )";
        }

        return args_image;
    }

    const PandaString &ImageOfType(const Type &type)
    {
        auto cached = cached_images_.find(type.Number());
        if (cached != cached_images_.end()) {
            return cached->second;
        }

        PandaString sort_name {(*sort_names_)[type.Sort(type_system_)]};

        const auto &params = type.GetTypeArgs(type_system_);

        auto &&params_image = ImageOfTypeArgs(params);

        PandaString val = sort_name + params_image;

        cached_images_[type.Number()] = val;

        return cached_images_[type.Number()];
    }

    const PandaString &operator[](const Type &type)
    {
        return ImageOfType(type);
    }

private:
    SortNames const *sort_names_;
    TypeSystem const *type_system_;
    PandaUnorderedMap<TypeNum, PandaString> cached_images_;
};
}  // namespace panda::verifier

#endif  // !_PANDA_TYPE_SYSTEM_HPP
