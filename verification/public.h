/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_VERIFICATION_PUBLIC_H_
#define PANDA_VERIFICATION_PUBLIC_H_

#include "runtime/include/mem/allocator.h"
#include "runtime/include/method.h"
#include <functional>
#include <string_view>

namespace panda::verifier {

typedef struct Config Config;

Config *NewConfig();
bool LoadConfigFile(Config *config, std::string_view config_file_name);
void DestroyConfig(Config *config);

bool ShouldSkip(Config const *config, panda::Method const *method);
bool IsEnabled(Config const *config);
bool IsOnlyVerify(Config const *config);
bool NeedShowStatus(Config const *config);

typedef struct Service Service;

Service *CreateService(Config const *config, int num_threads, panda::mem::InternalAllocatorPtr allocator,
                       std::string const &cache_file_name);
void StartService(Service *service);
void DestroyService(Service *service, bool update_cache_file);

Config const *GetServiceConfig(Service *service);

void CacheLibraryFiles(Service *service, PandaVector<panda_file::File const *> const &files);

bool Submit(Service *service, panda::Method *method);

typedef std::function<bool(panda::Method *)> WaitHandler;
typedef std::function<void(panda::Method *)> FailureHandler;

void Wait(Service *service, panda::Method *method, WaitHandler &&need_to_wait_more, FailureHandler &&handle_failure);

void SetVerified(Service *service, panda::Method *method, bool result);

enum class Status { OK, FAILED, UNKNOWN };

Status GetResult(Service *service, panda::Method *method);

void AddToDebugContext(panda::Method const *method, bool is_debug);
void DestroyDebugContext();

}  // namespace panda::verifier

#endif  // PANDA_VERIFICATION_PUBLIC_H_
