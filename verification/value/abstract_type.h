/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _PANDA_VERIFIER_ABSTRACT_TYPE_HPP
#define _PANDA_VERIFIER_ABSTRACT_TYPE_HPP

#include "verification/value/variables.h"
#include "verification/type/type_set.h"
#include "verification/type/type_system.h"

#include "macros.h"

#include <variant>

namespace panda::verifier {
class AbstractType {
public:
    struct None {};
    using ContentsData = std::variant<None, Variables::Var, Type, TypeSet>;
    AbstractType() = default;
    AbstractType(const AbstractType &) = default;
    AbstractType(AbstractType &&) = default;
    AbstractType(const Type &type) : Contents_ {type} {}
    AbstractType(const Variables::Var &var) : Contents_ {var} {}
    AbstractType(Variables::Var &&var) : Contents_ {std::move(var)} {}
    AbstractType(TypeSet &&type_set)
    {
        auto the_only_type = type_set.TheOnlyType();
        if (the_only_type.IsValid()) {
            Contents_ = the_only_type;
        } else {
            Contents_ = std::move(type_set);
        }
    }

    AbstractType &operator=(const AbstractType &) = default;
    AbstractType &operator=(AbstractType &&) = default;
    ~AbstractType() = default;
    AbstractType &operator=(const None &)
    {
        Contents_ = None {};
        return *this;
    }
    AbstractType &operator=(Variables::Var var)
    {
        Contents_ = var;
        return *this;
    }
    AbstractType &operator=(Type type)
    {
        Contents_ = type;
        return *this;
    }
    AbstractType &operator=(TypeSet &&type_set)
    {
        Contents_ = std::move(type_set);
        return *this;
    }

    Variables::Var GetVar() const
    {
        ASSERT(IsVar());
        return std::get<Variables::Var>(Contents_);
    }
    Type GetType() const
    {
        ASSERT(IsType());
        return std::get<Type>(Contents_);
    }
    const TypeSet &GetTypeSet() const
    {
        ASSERT(IsTypeSet());
        return std::get<TypeSet>(Contents_);
    }

    bool IsNone() const
    {
        return std::holds_alternative<None>(Contents_);
    }
    bool IsVar() const
    {
        return std::holds_alternative<Variables::Var>(Contents_);
    }
    bool IsType() const
    {
        return std::holds_alternative<Type>(Contents_);
    }
    bool IsTypeSet() const
    {
        return std::holds_alternative<TypeSet>(Contents_);
    }

    bool IsConsistent(TypeSystem const *tsys) const
    {
        if (IsType()) {
            return !tsys->IsTop(GetType());
        } else if (IsTypeSet()) {
            Type the_only_type = GetTypeSet().TheOnlyType();
            return !(the_only_type.IsValid() && tsys->IsTop(the_only_type));
        } else {
            return false;
        }
    }

    template <typename TypeImageFunc>
    PandaString Image(TypeImageFunc type_img_func) const
    {
        if (IsNone()) {
            return "<none>";
        } else if (IsVar()) {
            return GetVar().Image("<TypeVar") + ">";
        } else if (IsType()) {
            PandaString result = type_img_func(GetType());
            return result;
        } else if (IsTypeSet()) {
            return GetTypeSet().Image(type_img_func);
        }
        return "<unexpected kind of AbstractType>";
    }

    template <typename TypeHandler, typename Default>
    bool ForAllTypes(TypeHandler &&type_handler, Default &&non_type_handler) const
    {
        if (IsType()) {
            return type_handler(GetType());
        } else if (IsTypeSet()) {
            return GetTypeSet().ForAll(std::forward<TypeHandler>(type_handler));
        } else {
            return non_type_handler();
        }
    }

    template <typename TypeHandler>
    bool ForAllTypes(TypeHandler &&type_handler) const
    {
        return ForAllTypes(std::forward<TypeHandler>(type_handler), []() { return true; });
    }

    template <typename TypeHandler>
    bool ExistsType(TypeHandler &&type_handler) const
    {
        return !ForAllTypes([&type_handler](auto t) { return !type_handler(t); });
    }

private:
    ContentsData Contents_;

    friend AbstractType AtpJoin(AbstractType const *lhs, AbstractType const *rhs, TypeSystem const *tsys);
};

static AbstractType MergeTypeAndTypeSet(Type type, TypeSet const *type_set, TypeSystem const *tsys)
{
    if (type_set->Contains(type)) {
        return type;
    } else {
        auto supertypes = type.AllSupertypes(tsys);
        return TsIntersection(&supertypes, type_set, tsys);
    }
}

inline AbstractType AtpJoin(AbstractType const *lhs, AbstractType const *rhs, TypeSystem const *tsys)
{
    if (lhs->IsType()) {
        if (rhs->IsType()) {
            Type lhs_type = lhs->GetType();
            Type rhs_type = rhs->GetType();
            if (IsSubtype(lhs_type, rhs_type, tsys)) {
                return *rhs;
            } else if (IsSubtype(rhs_type, lhs_type, tsys)) {
                return *lhs;
            } else {
                auto lhs_supers = lhs_type.AllSupertypes(tsys);
                auto rhs_supers = rhs_type.AllSupertypes(tsys);
                return TsIntersection(&lhs_supers, &rhs_supers, tsys);
            }
        } else if (rhs->IsTypeSet()) {
            return MergeTypeAndTypeSet(lhs->GetType(), &rhs->GetTypeSet(), tsys);
        } else {
            UNREACHABLE();
        }
    } else if (lhs->IsTypeSet()) {
        if (rhs->IsType()) {
            return MergeTypeAndTypeSet(rhs->GetType(), &lhs->GetTypeSet(), tsys);
        } else if (rhs->IsTypeSet()) {
            return TsIntersection(&lhs->GetTypeSet(), &rhs->GetTypeSet(), tsys);
        } else {
            UNREACHABLE();
        }
    } else {
        UNREACHABLE();
    }
}

}  // namespace panda::verifier

#endif  // !_PANDA_VERIFIER_ABSTRACT_TYPE_HPP
