/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PANDA_VERIFICATION_DEFAULT_PLUGIN_H__
#define PANDA_VERIFICATION_DEFAULT_PLUGIN_H__

#include "abs_int_inl_compat_checks.h"
#include "verification/jobs/cache.h"
#include "verification/plugins.h"

namespace panda::verifier::plugin {

class DefaultPlugin final : public Plugin {
public:
    void TypeSystemSetup(PandaTypes *types) const override;

    CheckResult const &CheckFieldAccessViolation([[maybe_unused]] CachedField const *field,
                                                 [[maybe_unused]] CachedMethod const *from,
                                                 [[maybe_unused]] PandaTypes *types) const override
    {
        return CheckResult::ok;
    }

    CheckResult const &CheckMethodAccessViolation([[maybe_unused]] CachedMethod const *method,
                                                  [[maybe_unused]] CachedMethod const *from,
                                                  [[maybe_unused]] PandaTypes *types) const override
    {
        return CheckResult::ok;
    }

    CheckResult const &CheckClassAccessViolation([[maybe_unused]] CachedClass const *method,
                                                 [[maybe_unused]] CachedMethod const *from,
                                                 [[maybe_unused]] PandaTypes *types) const override
    {
        return CheckResult::ok;
    }

    Type TypeOfClass(CachedClass const *klass, PandaTypes *types) const override;
    Type TypeOfMethod(CachedMethod const *method, PandaTypes *types) const override;
    Type TypeOfTypeId(panda_file::Type::TypeId id, PandaTypes *types) const override;

    Type NormalizeType(Type type, PandaTypes *types) const override;
};

}  // namespace panda::verifier::plugin

#endif
