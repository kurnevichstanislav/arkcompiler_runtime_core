From c455c34f2f007f183cef8084e998047193adf5c2 Mon Sep 17 00:00:00 2001
From: a00533735 <>
Date: Mon, 20 Jun 2022 20:05:41 +0300
Subject: [PATCH 22/52] Make Sweep more general

TicketNo:_internal_
Description:Make Sweep more general
Now Sweep sweeps not only string table.
Some VMs may sweep other internal structures.
Team:ARK
Feature or Bugfix:Bugfix
Binary Source:No
PrivateCode(Yes/No):No

Change-Id: I047d471d70bb08dfa0dd389d23d815533ff54159
---
 plugins/ecmascript/runtime/ecma_vm.cpp  |  14 ++++
 plugins/ecmascript/runtime/ecma_vm.h    |   5 +-
 runtime/include/panda_vm.h              |   2 +-
 runtime/mem/gc/g1/g1-gc.cpp             |   7 +-
 runtime/mem/gc/gc_phase.h               |   2 -
 runtime/mem/gc/gc_scoped_phase.h        |   8 --
 runtime/mem/gc/gen-gc/gen-gc.cpp        | 101 ++++++++++--------------
 runtime/mem/gc/gen-gc/gen-gc.h          |  12 +--
 runtime/mem/gc/generational-gc-base.cpp |  13 ---
 runtime/mem/gc/generational-gc-base.h   |   5 --
 runtime/mem/gc/stw-gc/stw-gc.cpp        |  19 +----
 runtime/mem/gc/stw-gc/stw-gc.h          |   1 -
 12 files changed, 71 insertions(+), 118 deletions(-)

diff --git a/plugins/ecmascript/runtime/ecma_vm.cpp b/plugins/ecmascript/runtime/ecma_vm.cpp
index 06a0581b78..ec567e0442 100644
--- a/plugins/ecmascript/runtime/ecma_vm.cpp
+++ b/plugins/ecmascript/runtime/ecma_vm.cpp
@@ -546,6 +546,20 @@ bool EcmaVM::Execute(std::unique_ptr<const panda_file::File> pf, std::string_vie
     return true;
 }
 
+void EcmaVM::Sweep(const GCObjectVisitor &gc_object_visitor)
+{
+    GetEcmaStringTable()->Sweep(gc_object_visitor);
+    auto it = finalization_registries_.begin();
+    while (it != finalization_registries_.end()) {
+        if (gc_object_visitor(*it) == ObjectStatus::DEAD_OBJECT) {
+            auto to_remove = it++;
+            finalization_registries_.erase(to_remove);
+        } else {
+            ++it;
+        }
+    }
+}
+
 JSHandle<GlobalEnv> EcmaVM::GetGlobalEnv() const
 {
     return JSHandle<GlobalEnv>(reinterpret_cast<uintptr_t>(&globalEnv_));
diff --git a/plugins/ecmascript/runtime/ecma_vm.h b/plugins/ecmascript/runtime/ecma_vm.h
index 6ecdf40b0b..e8dbc171fa 100644
--- a/plugins/ecmascript/runtime/ecma_vm.h
+++ b/plugins/ecmascript/runtime/ecma_vm.h
@@ -226,10 +226,7 @@ public:
         GetEcmaStringTable()->VisitRoots(visitor, flags);
     }
 
-    void SweepStringTable(const GCObjectVisitor &gc_object_visitor) override
-    {
-        GetEcmaStringTable()->Sweep(gc_object_visitor);
-    }
+    void Sweep(const GCObjectVisitor &gc_object_visitor) override;
 
     bool UpdateMovedStrings() override
     {
diff --git a/runtime/include/panda_vm.h b/runtime/include/panda_vm.h
index e21be08027..57b652a5a3 100644
--- a/runtime/include/panda_vm.h
+++ b/runtime/include/panda_vm.h
@@ -107,7 +107,7 @@ public:
         GetStringTable()->VisitRoots(visitor, flags);
     }
 
-    virtual void SweepStringTable(const GCObjectVisitor &gc_object_visitor)
+    virtual void Sweep(const GCObjectVisitor &gc_object_visitor)
     {
         GetStringTable()->Sweep(gc_object_visitor);
     }
diff --git a/runtime/mem/gc/g1/g1-gc.cpp b/runtime/mem/gc/g1/g1-gc.cpp
index 80ad65e985..5f896c80ea 100644
--- a/runtime/mem/gc/g1/g1-gc.cpp
+++ b/runtime/mem/gc/g1/g1-gc.cpp
@@ -993,7 +993,12 @@ bool G1GC<LanguageConfig>::CollectAndMove(const CollectionSet &collection_set)
     PandaVector<Region *> tenured_regions(collection_set.Tenured().begin(), collection_set.Tenured().end());
     UpdateRefsToMovedObjects(&moved_objects_vector);
     this->VerifyCollectAndMove(std::move(collect_verifier), collection_set);
-    this->SweepStringTableYoung([this](ObjectHeader *obj) { return this->InGCSweepRange(obj); });
+    this->GetPandaVm()->Sweep([this](ObjectHeader *obj) {
+        if (this->InGCSweepRange(obj)) {
+            return ObjectStatus::DEAD_OBJECT;
+        }
+        return ObjectStatus::ALIVE_OBJECT;
+    });
 
     ActualizeRemSets();
 
diff --git a/runtime/mem/gc/gc_phase.h b/runtime/mem/gc/gc_phase.h
index a53d5edcc6..1d93f229a5 100644
--- a/runtime/mem/gc/gc_phase.h
+++ b/runtime/mem/gc/gc_phase.h
@@ -26,8 +26,6 @@ enum class GCPhase {
     GC_PHASE_MARK_YOUNG,
     GC_PHASE_REMARK,
     GC_PHASE_COLLECT_YOUNG_AND_MOVE,
-    GC_PHASE_SWEEP_STRING_TABLE,
-    GC_PHASE_SWEEP_STRING_TABLE_YOUNG,
     GC_PHASE_SWEEP,
     GC_PHASE_CLEANUP,
     GC_PHASE_LAST
diff --git a/runtime/mem/gc/gc_scoped_phase.h b/runtime/mem/gc/gc_scoped_phase.h
index cbe214cbb8..7fda82e5f5 100644
--- a/runtime/mem/gc/gc_scoped_phase.h
+++ b/runtime/mem/gc/gc_scoped_phase.h
@@ -54,10 +54,6 @@ public:
                 return "YoungRemark()";
             case GCPhase::GC_PHASE_COLLECT_YOUNG_AND_MOVE:
                 return "CollectAndMove()";
-            case GCPhase::GC_PHASE_SWEEP_STRING_TABLE:
-                return "SweepStringTable()";
-            case GCPhase::GC_PHASE_SWEEP_STRING_TABLE_YOUNG:
-                return "SweepStringTableYoung()";
             case GCPhase::GC_PHASE_SWEEP:
                 return "Sweep()";
             case GCPhase::GC_PHASE_CLEANUP:
@@ -86,10 +82,6 @@ public:
                 return "YRemark";
             case GCPhase::GC_PHASE_COLLECT_YOUNG_AND_MOVE:
                 return "ColYAndMove";
-            case GCPhase::GC_PHASE_SWEEP_STRING_TABLE:
-                return "SweepStrT";
-            case GCPhase::GC_PHASE_SWEEP_STRING_TABLE_YOUNG:
-                return "SweepStrTY()";
             case GCPhase::GC_PHASE_SWEEP:
                 return "Sweep";
             case GCPhase::GC_PHASE_CLEANUP:
diff --git a/runtime/mem/gc/gen-gc/gen-gc.cpp b/runtime/mem/gc/gen-gc/gen-gc.cpp
index cdb41430eb..6e377d7f61 100644
--- a/runtime/mem/gc/gen-gc/gen-gc.cpp
+++ b/runtime/mem/gc/gen-gc/gen-gc.cpp
@@ -354,10 +354,7 @@ void GenGC<LanguageConfig>::CollectYoungAndMove()
     this->mem_stats_.RecordCountFreedYoung(young_delete_count);
     UpdateRefsToMovedObjects(&moved_objects);
     this->VerifyCollectAndMove(std::move(young_verifier));
-    // Sweep string table here to avoid dangling references
-    this->SweepStringTableYoung([&young_mem_range](ObjectHeader *object_header) {
-        return young_mem_range.IsAddressInRange(ToUintPtr(object_header));
-    });
+    SweepYoung();
     // Remove young
     object_allocator->ResetYoungAllocator();
 
@@ -366,22 +363,6 @@ void GenGC<LanguageConfig>::CollectYoungAndMove()
     LOG_DEBUG_GC << "== GenGC CollectYoungAndMove end ==";
 }
 
-template <class LanguageConfig>
-void GenGC<LanguageConfig>::SweepStringTable()
-{
-    GCScope<TRACE_TIMING_PHASE> scope(__FUNCTION__, this, GCPhase::GC_PHASE_SWEEP_STRING_TABLE);
-
-    ASSERT(this->GetObjectAllocator()->GetYoungSpaceMemRanges().size() == 1);
-    // new strings may be created in young space during tenured gc, we shouldn't collect them
-    auto young_mem_range = this->GetObjectAllocator()->GetYoungSpaceMemRanges().at(0);
-    this->GetPandaVm()->SweepStringTable([this, &young_mem_range](ObjectHeader *object) {
-        if (young_mem_range.IsAddressInRange(ToUintPtr(object))) {
-            return ObjectStatus::ALIVE_OBJECT;
-        }
-        return this->marker_.MarkChecker(object);
-    });
-}
-
 template <class LanguageConfig>
 void GenGC<LanguageConfig>::UpdateRefsToMovedObjects(PandaVector<ObjectHeader *> *moved_objects)
 {
@@ -442,9 +423,7 @@ void GenGC<LanguageConfig>::RunTenuredGC(GCTask &task)
         // TODO(yxr): remove this after not marking young objects in tenured gc
         this->GetObjectAllocator()->IterateOverYoungObjects([this](ObjectHeader *obj) { this->marker_.UnMark(obj); });
     }
-    // TODO(dtrubenkov): make concurrent
-    SweepStringTable();
-    ConcurrentSweep();
+    Sweep<true>();
     this->GetPandaVm()->GetMemStats()->RecordGCPauseEnd();
     LOG_DEBUG_GC << "GC tenured end";
     task.collection_type_ = GCCollectionType::TENURED;
@@ -463,9 +442,7 @@ void GenGC<LanguageConfig>::RunFullGC(GCTask &task)
         this->GetObjectAllocator()->IterateOverObjects([this](ObjectHeader *obj) { this->marker_.UnMark(obj); });
     }
     FullMark(task);
-    // Sweep dead objects from tenured space
-    SweepStringTable();
-    Sweep();
+    Sweep<false>();
     // Young GC
     if (LIKELY(HaveEnoughSpaceToMove())) {
         GCScopedPauseStats scoped_pause_stats(this->GetPandaVm()->GetGCStats());  // Not clear young pause
@@ -623,35 +600,10 @@ bool GenGC<LanguageConfig>::IsMarked(const ObjectHeader *object) const
     return this->marker_.IsMarked(object);
 }
 
-template <class LanguageConfig>
-void GenGC<LanguageConfig>::Sweep()
-{
-    GCScope<TRACE_TIMING_PHASE> gc_sweep_scope(__FUNCTION__, this, GCPhase::GC_PHASE_SWEEP);
-
-    size_t freed_object_size = 0U;
-    size_t freed_object_count = 0U;
-
-    this->GetObjectAllocator()->Collect(
-        [this, &freed_object_size, &freed_object_count](ObjectHeader *object) {
-            auto status = this->marker_.MarkChecker(object);
-            if (status == ObjectStatus::DEAD_OBJECT) {
-                freed_object_size += GetAlignedObjectSize(GetObjectSize(object));
-                freed_object_count++;
-            }
-            return status;
-        },
-        GCCollectMode::GC_ALL);
-    this->GetObjectAllocator()->VisitAndRemoveFreePools([this](void *mem, size_t size) {
-        this->GetCardTable()->ClearCardRange(ToUintPtr(mem), ToUintPtr(mem) + size);
-        PoolManager::GetMmapMemPool()->FreePool(mem, size);
-    });
-    this->mem_stats_.RecordSizeFreedTenured(freed_object_size);
-    this->mem_stats_.RecordCountFreedTenured(freed_object_count);
-}
-
 // NO_THREAD_SAFETY_ANALYSIS because clang thread safety analysis
 template <class LanguageConfig>
-NO_THREAD_SAFETY_ANALYSIS void GenGC<LanguageConfig>::ConcurrentSweep()
+template <bool concurrent>
+NO_THREAD_SAFETY_ANALYSIS void GenGC<LanguageConfig>::Sweep()
 {
     GCScope<TRACE_TIMING> gc_scope(__FUNCTION__, this);
     ConcurrentScope concurrent_scope(this, false);
@@ -661,10 +613,22 @@ NO_THREAD_SAFETY_ANALYSIS void GenGC<LanguageConfig>::ConcurrentSweep()
     // NB! can't move block out of brace, we need to make sure GC_PHASE_SWEEP cleared
     {
         GCScopedPhase scoped_phase(this->GetPandaVm()->GetMemStats(), this, GCPhase::GC_PHASE_SWEEP);
-        concurrent_scope.Start();  // enable concurrent after GC_PHASE_SWEEP has been set
+        // TODO(dtrubenkov): make concurrent
+        ASSERT(this->GetObjectAllocator()->GetYoungSpaceMemRanges().size() == 1);
+        // new strings may be created in young space during tenured gc, we shouldn't collect them
+        auto young_mem_range = this->GetObjectAllocator()->GetYoungSpaceMemRanges().at(0);
+        this->GetPandaVm()->Sweep([this, &young_mem_range](ObjectHeader *object) {
+            if (young_mem_range.IsAddressInRange(ToUintPtr(object))) {
+                return ObjectStatus::ALIVE_OBJECT;
+            }
+            return this->marker_.MarkChecker(object);
+        });
+        if constexpr (concurrent) {
+            concurrent_scope.Start();  // enable concurrent after GC_PHASE_SWEEP has been set
+        }
 
         // NOLINTNEXTLINE(readability-braces-around-statements, bugprone-suspicious-semicolon)
-        if constexpr (LanguageConfig::MT_MODE == MT_MODE_MULTI) {
+        if constexpr (concurrent && LanguageConfig::MT_MODE == MT_MODE_MULTI) {
             // Run monitor deflation again, to avoid object was reclaimed before monitor deflate.
             auto young_mr = this->GetObjectAllocator()->GetYoungSpaceMemRanges().at(0);
             this->GetPandaVm()->GetMonitorPool()->DeflateMonitorsWithCallBack([&young_mr, this](Monitor *monitor) {
@@ -693,10 +657,29 @@ NO_THREAD_SAFETY_ANALYSIS void GenGC<LanguageConfig>::ConcurrentSweep()
     this->mem_stats_.RecordSizeFreedTenured(freed_object_size);
     this->mem_stats_.RecordCountFreedTenured(freed_object_count);
 
-    // In concurrent sweep phase, the new created objects may being marked in InitGCBits,
-    // so we need wait for that done, then we can safely unmark objects concurrent with mutator.
-    ASSERT(this->GetGCPhase() != GCPhase::GC_PHASE_SWEEP);  // Make sure we are out of sweep scope
-    this->GetObjectAllocator()->IterateOverTenuredObjects([this](ObjectHeader *obj) { this->marker_.UnMark(obj); });
+    if constexpr (concurrent) {
+        // In concurrent sweep phase, the new created objects may being marked in InitGCBits,
+        // so we need wait for that done, then we can safely unmark objects concurrent with mutator.
+        ASSERT(this->GetGCPhase() != GCPhase::GC_PHASE_SWEEP);  // Make sure we are out of sweep scope
+        this->GetObjectAllocator()->IterateOverTenuredObjects([this](ObjectHeader *obj) { this->marker_.UnMark(obj); });
+    }
+}
+
+template <class LanguageConfig>
+void GenGC<LanguageConfig>::SweepYoung()
+{
+    GCScope<TRACE_TIMING_PHASE> scope(__FUNCTION__, this, GCPhase::GC_PHASE_SWEEP);
+    // new strings may be created in young space during tenured gc, we shouldn't collect them
+    // Sweep string table here to avoid dangling references
+    ASSERT(this->GetObjectAllocator()->GetYoungSpaceMemRanges().size() == 1);
+    // new strings may be created in young space during tenured gc, we shouldn't collect them
+    auto young_mem_range = this->GetObjectAllocator()->GetYoungSpaceMemRanges().at(0);
+    this->GetPandaVm()->Sweep([&young_mem_range](ObjectHeader *object_header) {
+        if (young_mem_range.IsAddressInRange(ToUintPtr(object_header))) {
+            return ObjectStatus::DEAD_OBJECT;
+        }
+        return ObjectStatus::ALIVE_OBJECT;
+    });
 }
 
 template <class LanguageConfig>
diff --git a/runtime/mem/gc/gen-gc/gen-gc.h b/runtime/mem/gc/gen-gc/gen-gc.h
index d8d450d6a5..1df592efbe 100644
--- a/runtime/mem/gc/gen-gc/gen-gc.h
+++ b/runtime/mem/gc/gen-gc/gen-gc.h
@@ -137,25 +137,21 @@ private:
      */
     void VerifyCollectAndMove(HeapVerifierIntoGC<LanguageConfig> &&young_verifier);
 
-    /**
-     * Remove dead strings from string table in tenured space
-     */
-    void SweepStringTable();
-
     /**
      * Update all refs to moved objects
      */
     void UpdateRefsToMovedObjects(PandaVector<ObjectHeader *> *moved_objects);
 
     /**
-     * Sweep dead objects in tenured space on pause
+     * Sweep dead objects in tenured space
      */
+    template <bool concurrent>
     void Sweep();
 
     /**
-     * Concurrent sweep dead objects in tenured space
+     * Sweep dead objects in young space
      */
-    void ConcurrentSweep();
+    void SweepYoung();
 
     bool IsMarked(const ObjectHeader *object) const override;
 
diff --git a/runtime/mem/gc/generational-gc-base.cpp b/runtime/mem/gc/generational-gc-base.cpp
index d7150eea5f..198f14221a 100644
--- a/runtime/mem/gc/generational-gc-base.cpp
+++ b/runtime/mem/gc/generational-gc-base.cpp
@@ -133,19 +133,6 @@ void GenerationalGC<LanguageConfig>::UpdateMemStats(size_t bytes_in_heap_before,
                                     SpaceType::SPACE_TYPE_OBJECT);
 }
 
-template <class LanguageConfig>
-void GenerationalGC<LanguageConfig>::SweepStringTableYoung(const std::function<bool(ObjectHeader *)> &young_checker)
-{
-    GCScope<TRACE_TIMING_PHASE> scoped_trace(__FUNCTION__, this, GCPhase::GC_PHASE_SWEEP_STRING_TABLE_YOUNG);
-
-    this->GetPandaVm()->SweepStringTable(static_cast<GCObjectVisitor>([&young_checker](ObjectHeader *object_header) {
-        if (young_checker(object_header)) {
-            return ObjectStatus::DEAD_OBJECT;
-        }
-        return ObjectStatus::ALIVE_OBJECT;
-    }));
-}
-
 template <class LanguageConfig>
 void GenerationalGC<LanguageConfig>::CreateCardTable(InternalAllocatorPtr internal_allocator_ptr, uintptr_t min_address,
                                                      size_t size)
diff --git a/runtime/mem/gc/generational-gc-base.h b/runtime/mem/gc/generational-gc-base.h
index 41b1ed4fe8..ba0d0cb8e0 100644
--- a/runtime/mem/gc/generational-gc-base.h
+++ b/runtime/mem/gc/generational-gc-base.h
@@ -306,11 +306,6 @@ protected:
         return static_cast<ObjectAllocatorGenBase *>(this->GetObjectAllocator());
     }
 
-    /**
-     * Sweeps string table from about to become dangled pointers to young generation
-     */
-    void SweepStringTableYoung(const std::function<bool(ObjectHeader *)> &young_checker);
-
     void CreateCardTable(InternalAllocatorPtr internal_allocator_ptr, uintptr_t min_address, size_t size);
 
     MemStats mem_stats_;  // NOLINT(misc-non-private-member-variables-in-classes)
diff --git a/runtime/mem/gc/stw-gc/stw-gc.cpp b/runtime/mem/gc/stw-gc/stw-gc.cpp
index 9f3eca51dc..3b89702b49 100644
--- a/runtime/mem/gc/stw-gc/stw-gc.cpp
+++ b/runtime/mem/gc/stw-gc/stw-gc.cpp
@@ -66,7 +66,6 @@ void StwGC<LanguageConfig>::RunPhasesImpl(GCTask &task)
     [[maybe_unused]] size_t bytes_in_heap_before_gc = this->GetPandaVm()->GetMemStats()->GetFootprintHeap();
     marker_.BindBitmaps(true);
     Mark(task);
-    SweepStringTable();
     Sweep();
     marker_.ReverseMark();
     [[maybe_unused]] size_t bytes_in_heap_after_gc = this->GetPandaVm()->GetMemStats()->GetFootprintHeap();
@@ -135,21 +134,6 @@ void StwGC<LanguageConfig>::MarkStack(GCMarkingStackType *stack, const GC::MarkP
     LOG_DEBUG_GC << "Iterated over " << objects_count << " objects in the stack";
 }
 
-template <class LanguageConfig>
-void StwGC<LanguageConfig>::SweepStringTable()
-{
-    GCScope<TRACE_PHASE> gc_scope(__FUNCTION__, this, GCPhase::GC_PHASE_SWEEP_STRING_TABLE);
-    auto vm = this->GetPandaVm();
-
-    if (!marker_.IsReverseMark()) {
-        LOG_DEBUG_GC << "SweepStringTable with MarkChecker";
-        vm->SweepStringTable([this](ObjectHeader *object) { return this->marker_.MarkChecker(object); });
-    } else {
-        LOG_DEBUG_GC << "SweepStringTable with ReverseMarkChecker";
-        vm->SweepStringTable([this](ObjectHeader *object) { return this->marker_.template MarkChecker<true>(object); });
-    }
-}
-
 template <class LanguageConfig>
 void StwGC<LanguageConfig>::Sweep()
 {
@@ -158,6 +142,7 @@ void StwGC<LanguageConfig>::Sweep()
     // TODO(dtrubenk): add other allocators when they will be used/implemented
     if (!marker_.IsReverseMark()) {
         LOG_DEBUG_GC << "Sweep with MarkChecker";
+        this->GetPandaVm()->Sweep([this](ObjectHeader *object) { return this->marker_.MarkChecker(object); });
         this->GetObjectAllocator()->Collect(
             [this](ObjectHeader *object) {
                 auto status = this->marker_.MarkChecker(object);
@@ -172,6 +157,8 @@ void StwGC<LanguageConfig>::Sweep()
 
     } else {
         LOG_DEBUG_GC << "Sweep with ReverseMarkChecker";
+        this->GetPandaVm()->Sweep(
+            [this](ObjectHeader *object) { return this->marker_.template MarkChecker<true>(object); });
         this->GetObjectAllocator()->Collect(
             [this](ObjectHeader *object) {
                 auto status = this->marker_.template MarkChecker<true>(object);
diff --git a/runtime/mem/gc/stw-gc/stw-gc.h b/runtime/mem/gc/stw-gc/stw-gc.h
index a452ec84c8..ad82d77992 100644
--- a/runtime/mem/gc/stw-gc/stw-gc.h
+++ b/runtime/mem/gc/stw-gc/stw-gc.h
@@ -101,7 +101,6 @@ private:
     void RunPhasesImpl(GCTask &task) override;
     void Mark(const GCTask &task);
     void MarkStack(GCMarkingStackType *stack, const GC::MarkPredicate &markPredicate);
-    void SweepStringTable();
     void Sweep();
 
     void MarkObject(ObjectHeader *object) override;
-- 
2.17.1

