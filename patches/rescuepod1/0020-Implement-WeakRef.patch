From fc497ff0db68250eea0c9bc4d08c3ed58194dd17 Mon Sep 17 00:00:00 2001
From: a00533735 <>
Date: Fri, 17 Jun 2022 18:16:27 +0300
Subject: [PATCH 20/52] Implement WeakRef

TicketNo:_internal_
Description:Implement WeakRef
Team:ARK
Feature or Bugfix:Bugfix
Binary Source:No
PrivateCode(Yes/No):No

Change-Id: I3fb72659b8d008c1c109a865bedd658a7ece8c3f
---
 plugins/ecmascript/runtime/CMakeLists.txt     |  1 +
 plugins/ecmascript/runtime/builtins.cpp       | 30 ++++++++
 plugins/ecmascript/runtime/builtins.h         |  2 +
 .../runtime/builtins/builtins_weak_ref.cpp    | 70 +++++++++++++++++++
 .../runtime/builtins/builtins_weak_ref.h      | 32 +++++++++
 plugins/ecmascript/runtime/dump.cpp           |  9 +++
 .../runtime/global_env_constants.cpp          |  5 ++
 .../ecmascript/runtime/global_env_constants.h |  1 +
 plugins/ecmascript/runtime/js_hclass.h        |  6 ++
 .../ecmascript/runtime/js_tagged_value-inl.h  |  5 ++
 plugins/ecmascript/runtime/js_tagged_value.h  |  1 +
 .../ecmascript/runtime/js_weak_container.h    | 15 ++++
 .../runtime/mem/ecma_reference_processor.cpp  | 15 +++-
 plugins/ecmascript/runtime/object_factory.cpp | 15 ++++
 plugins/ecmascript/runtime/object_factory.h   |  4 ++
 plugins/ecmascript/runtime/runtime_call_id.h  |  2 +
 plugins/ecmascript/runtime/runtime_sources.gn |  1 +
 17 files changed, 213 insertions(+), 1 deletion(-)
 create mode 100644 plugins/ecmascript/runtime/builtins/builtins_weak_ref.cpp
 create mode 100644 plugins/ecmascript/runtime/builtins/builtins_weak_ref.h

diff --git a/plugins/ecmascript/runtime/CMakeLists.txt b/plugins/ecmascript/runtime/CMakeLists.txt
index d1407c8979..12a74be205 100644
--- a/plugins/ecmascript/runtime/CMakeLists.txt
+++ b/plugins/ecmascript/runtime/CMakeLists.txt
@@ -85,6 +85,7 @@ set(ECMASCRIPT_SOURCES
     ${ECMA_SRC_DIR}/builtins/builtins_string_iterator.cpp
     ${ECMA_SRC_DIR}/builtins/builtins_symbol.cpp
     ${ECMA_SRC_DIR}/builtins/builtins_typedarray.cpp
+    ${ECMA_SRC_DIR}/builtins/builtins_weak_ref.cpp
     ${ECMA_SRC_DIR}/builtins/builtins_weak_map.cpp
     ${ECMA_SRC_DIR}/builtins/builtins_weak_set.cpp
     ${ECMA_SRC_DIR}/class_linker/panda_file_translator.cpp
diff --git a/plugins/ecmascript/runtime/builtins.cpp b/plugins/ecmascript/runtime/builtins.cpp
index 123b8456d1..4888142e76 100644
--- a/plugins/ecmascript/runtime/builtins.cpp
+++ b/plugins/ecmascript/runtime/builtins.cpp
@@ -56,6 +56,7 @@
 #include "plugins/ecmascript/runtime/builtins/builtins_string_iterator.h"
 #include "plugins/ecmascript/runtime/builtins/builtins_symbol.h"
 #include "plugins/ecmascript/runtime/builtins/builtins_typedarray.h"
+#include "plugins/ecmascript/runtime/builtins/builtins_weak_ref.h"
 #include "plugins/ecmascript/runtime/builtins/builtins_weak_map.h"
 #include "plugins/ecmascript/runtime/builtins/builtins_weak_set.h"
 #include "plugins/ecmascript/runtime/containers/containers_private.h"
@@ -102,6 +103,7 @@ using Symbol = builtins::BuiltinsSymbol;
 using Boolean = builtins::BuiltinsBoolean;
 using BuiltinsMap = builtins::BuiltinsMap;
 using BuiltinsSet = builtins::BuiltinsSet;
+using BuiltinsWeakRef = builtins::BuiltinsWeakRef;
 using BuiltinsWeakMap = builtins::BuiltinsWeakMap;
 using BuiltinsWeakSet = builtins::BuiltinsWeakSet;
 using BuiltinsArray = builtins::BuiltinsArray;
@@ -279,6 +281,7 @@ void Builtins::Initialize(const JSHandle<GlobalEnv> &env, JSThread *thread)
     InitializeRegExp(env);
     InitializeSet(env, objFuncDynclass);
     InitializeMap(env, objFuncDynclass);
+    InitializeWeakRef(env, objFuncDynclass);
     InitializeWeakMap(env, objFuncDynclass);
     InitializeWeakSet(env, objFuncDynclass);
     InitializeArray(env, objFuncPrototypeVal);
@@ -1255,6 +1258,33 @@ void Builtins::InitializeMap(const JSHandle<GlobalEnv> &env, const JSHandle<JSHC
     env->SetMapPrototype(thread_, mapFuncPrototype);
 }
 
+void Builtins::InitializeWeakRef(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &objFuncDynclass) const
+{
+    [[maybe_unused]] EcmaHandleScope scope(thread_);
+    const GlobalEnvConstants *globalConst = thread_->GlobalConstants();
+    // WeakRef.prototype
+    JSHandle<JSObject> weakRefFuncPrototype = factory_->NewJSObject(objFuncDynclass);
+    JSHandle<JSTaggedValue> weakRefFuncPrototypeValue(weakRefFuncPrototype);
+    // WeakRef.prototype_or_dynclass
+    JSHandle<JSHClass> weakRefFuncInstanceDynclass =
+        factory_->NewEcmaDynClass(JSWeakRef::SIZE, JSType::JS_WEAK_REF, weakRefFuncPrototypeValue);
+    weakRefFuncInstanceDynclass->SetWeakContainer(true);
+    // WeakRef() = new Function()
+    JSHandle<JSTaggedValue> weakRefFunction(NewBuiltinConstructor(
+        env, weakRefFuncPrototype, BuiltinsWeakRef::Constructor, "WeakRef", FunctionLength::ONE));
+    // WeakRef().prototype = WeakRef.Prototype & WeakRef.prototype.constructor = WeakRef()
+    JSFunction::Cast(weakRefFunction->GetTaggedObject())
+        ->SetProtoOrDynClass(thread_, weakRefFuncInstanceDynclass.GetTaggedValue());
+
+    // "constructor" property on the prototype
+    JSHandle<JSTaggedValue> constructorKey = globalConst->GetHandledConstructorString();
+    JSObject::SetProperty(thread_, JSHandle<JSTaggedValue>(weakRefFuncPrototype), constructorKey, weakRefFunction);
+    // weakref.prototype.deref()
+    SetFunction(env, weakRefFuncPrototype, "deref", BuiltinsWeakRef::Deref, FunctionLength::ZERO);
+    // @@ToStringTag
+    SetStringTagSymbol(env, weakRefFuncPrototype, "WeakRef");
+}
+
 void Builtins::InitializeWeakMap(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &objFuncDynclass) const
 {
     [[maybe_unused]] EcmaHandleScope scope(thread_);
diff --git a/plugins/ecmascript/runtime/builtins.h b/plugins/ecmascript/runtime/builtins.h
index d1431c42cf..fd064f14b1 100644
--- a/plugins/ecmascript/runtime/builtins.h
+++ b/plugins/ecmascript/runtime/builtins.h
@@ -128,6 +128,8 @@ private:
 
     void InitializeMap(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &objFuncDynclass) const;
 
+    void InitializeWeakRef(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &objFuncDynclass) const;
+
     void InitializeWeakMap(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &objFuncDynclass) const;
 
     void InitializeWeakSet(const JSHandle<GlobalEnv> &env, const JSHandle<JSHClass> &objFuncDynclass) const;
diff --git a/plugins/ecmascript/runtime/builtins/builtins_weak_ref.cpp b/plugins/ecmascript/runtime/builtins/builtins_weak_ref.cpp
new file mode 100644
index 0000000000..aa2200f1c2
--- /dev/null
+++ b/plugins/ecmascript/runtime/builtins/builtins_weak_ref.cpp
@@ -0,0 +1,70 @@
+/*
+ * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *     http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+#include "plugins/ecmascript/runtime/builtins/builtins_weak_ref.h"
+#include "plugins/ecmascript/runtime/ecma_vm.h"
+#include "plugins/ecmascript/runtime/global_env.h"
+#include "plugins/ecmascript/runtime/internal_call_params.h"
+#include "plugins/ecmascript/runtime/js_weak_container.h"
+#include "plugins/ecmascript/runtime/object_factory.h"
+
+namespace panda::ecmascript::builtins {
+JSTaggedValue BuiltinsWeakRef::Constructor(EcmaRuntimeCallInfo *argv)
+{
+    ASSERT(argv);
+    BUILTINS_API_TRACE(argv->GetThread(), WeakRef, Constructor);
+    JSThread *thread = argv->GetThread();
+    [[maybe_unused]] EcmaHandleScope handleScope(thread);
+    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
+    // 1.If NewTarget is undefined, throw a TypeError exception
+    JSHandle<JSTaggedValue> newTarget = GetNewTarget(argv);
+    if (newTarget->IsUndefined()) {
+        THROW_TYPE_ERROR_AND_RETURN(thread, "new target must not be undefined", JSTaggedValue::Exception());
+    }
+    // 2. If Type(target) is not Object, throw a TypeError exception.
+    if (argv->GetArgsNumber() < 1U || !GetCallArg(argv, 0U)->IsHeapObject()) {
+        THROW_TYPE_ERROR_AND_RETURN(thread, "target must not be undefined", JSTaggedValue::Exception());
+    }
+    JSHandle<JSTaggedValue> target = GetCallArg(argv, 0);
+    // 3. Let WeakRef be OrdinaryCreateFromConstructor(NewTarget, "%WeakRefPrototype%", «[[WeakRefTarget]]» ).
+    JSHandle<JSTaggedValue> constructor = GetConstructor(argv);
+    JSHandle<JSObject> obj = factory->NewJSObjectByConstructor(JSHandle<JSFunction>(constructor), newTarget);
+    RETURN_EXCEPTION_IF_ABRUPT_COMPLETION(thread);
+    JSHandle<JSWeakRef> weakRef = JSHandle<JSWeakRef>::Cast(obj);
+
+    // 4. Perform AddToKeptObjects(target).
+    // 5. Set weakRef.[[WeakRefTarget]] to target.
+    weakRef->SetReferent(thread, target.GetTaggedValue());
+    // Return weakRef.
+    return weakRef.GetTaggedValue();
+}
+
+JSTaggedValue BuiltinsWeakRef::Deref(EcmaRuntimeCallInfo *argv)
+{
+    ASSERT(argv);
+    BUILTINS_API_TRACE(argv->GetThread(), WeakRef, Deref);
+    JSThread *thread = argv->GetThread();
+    [[maybe_unused]] EcmaHandleScope handleScope(thread);
+    JSHandle<JSTaggedValue> self = GetThis(argv);
+    // 1. Let weakRef be the this value.
+    // 2. Perform ? RequireInternalSlot(weakRef, [[WeakRefTarget]]).
+    if (!self->IsJSWeakRef()) {
+        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not JSWeakRef.", JSTaggedValue::Exception());
+    }
+    JSHandle<JSWeakRef> weakRef(self);
+    // 3. Return WeakRefDeref(weakRef).
+    return weakRef->GetReferent();
+}
+}  // namespace panda::ecmascript::builtins
diff --git a/plugins/ecmascript/runtime/builtins/builtins_weak_ref.h b/plugins/ecmascript/runtime/builtins/builtins_weak_ref.h
new file mode 100644
index 0000000000..d60e94700c
--- /dev/null
+++ b/plugins/ecmascript/runtime/builtins/builtins_weak_ref.h
@@ -0,0 +1,32 @@
+/*
+ * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *     http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+#ifndef ECMASCRIPT_BUILTINS_BUILTINS_WEAK_REF_H
+#define ECMASCRIPT_BUILTINS_BUILTINS_WEAK_REF_H
+
+#include "plugins/ecmascript/runtime/base/builtins_base.h"
+#include "plugins/ecmascript/runtime/ecma_runtime_call_info.h"
+
+namespace panda::ecmascript::builtins {
+class BuiltinsWeakRef : public ecmascript::base::BuiltinsBase {
+public:
+    // 26.1.1.1
+    static JSTaggedValue Constructor(EcmaRuntimeCallInfo *argv);
+    // 26.1.3.2
+    static JSTaggedValue Deref(EcmaRuntimeCallInfo *argv);
+    // 26.1.3.3 @@toStringTag
+};
+}  // namespace panda::ecmascript::builtins
+#endif  // ECMASCRIPT_BUILTINS_BUILTINS_REF_H
diff --git a/plugins/ecmascript/runtime/dump.cpp b/plugins/ecmascript/runtime/dump.cpp
index 6b8fb9ad05..36b7f3fdf3 100755
--- a/plugins/ecmascript/runtime/dump.cpp
+++ b/plugins/ecmascript/runtime/dump.cpp
@@ -439,6 +439,9 @@ static void DumpObject(JSThread *thread, TaggedObject *obj, std::ostream &os)
         case JSType::JS_MAP:
             JSMap::Cast(obj)->Dump(thread, os);
             break;
+        case JSType::JS_WEAK_REF:
+            JSWeakRef::Cast(obj)->Dump(thread, os);
+            break;
         case JSType::JS_WEAK_SET:
             JSWeakSet::Cast(obj)->Dump(thread, os);
             break;
@@ -1071,6 +1074,12 @@ void JSSet::Dump(JSThread *thread, std::ostream &os) const
     set->Dump(thread, os);
 }
 
+void JSWeakRef::Dump(JSThread *thread, std::ostream &os) const
+{
+    os << " - referent: " << (GetReferent().IsUndefined() ? "undefined\n" : "<object>\n");
+    JSObject::Dump(thread, os);
+}
+
 void JSWeakMap::Dump(JSThread *thread, std::ostream &os) const
 {
     LinkedHashMap *map = LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject());
diff --git a/plugins/ecmascript/runtime/global_env_constants.cpp b/plugins/ecmascript/runtime/global_env_constants.cpp
index bda7c1b003..2a4fb76ae9 100644
--- a/plugins/ecmascript/runtime/global_env_constants.cpp
+++ b/plugins/ecmascript/runtime/global_env_constants.cpp
@@ -51,6 +51,7 @@
 #include "plugins/ecmascript/runtime/js_symbol.h"
 #include "plugins/ecmascript/runtime/js_tagged_value.h"
 #include "plugins/ecmascript/runtime/js_thread.h"
+#include "plugins/ecmascript/runtime/js_weak_container.h"
 #include "plugins/ecmascript/runtime/object_factory.h"
 #include "plugins/ecmascript/runtime/object_wrapper.h"
 
@@ -180,6 +181,10 @@ void GlobalEnvConstants::InitRootsClass([[maybe_unused]] JSThread *thread, JSHCl
     SetConstant(ConstantIndex::LINKED_HASH_MAP_CLASS_INDEX,
                 factory->NewEcmaDynClass(dynClassClass, 0, JSType::LINKED_HASH_MAP, HClass::ARRAY).GetTaggedValue());
 
+    JSHClass *weakRefClass = *factory->NewEcmaDynClass(dynClassClass, JSWeakRef::SIZE, JSType::JS_WEAK_REF, 0, 0);
+    weakRefClass->SetWeakContainer(true);
+    SetConstant(ConstantIndex::WEAK_REF_CLASS_INDEX, JSTaggedValue(weakRefClass));
+
     JSHClass *weakHashSetClass = *factory->NewEcmaDynClass(dynClassClass, 0, JSType::LINKED_HASH_SET, HClass::ARRAY);
     weakHashSetClass->SetWeakContainer(true);
     SetConstant(ConstantIndex::WEAK_LINKED_HASH_SET_CLASS_INDEX, JSTaggedValue(weakHashSetClass));
diff --git a/plugins/ecmascript/runtime/global_env_constants.h b/plugins/ecmascript/runtime/global_env_constants.h
index d4c192c6f4..d0eaa7b7c5 100755
--- a/plugins/ecmascript/runtime/global_env_constants.h
+++ b/plugins/ecmascript/runtime/global_env_constants.h
@@ -66,6 +66,7 @@ class JSThread;
     V(JSTaggedValue, JSProxyCallableClass, JS_PROXY_CALLABLE_CLASS_INDEX, ecma_roots_class)                           \
     V(JSTaggedValue, JSProxyConstructClass, JS_PROXY_CONSTRUCT_CLASS_INDEX, ecma_roots_class)                         \
     V(JSTaggedValue, JSRealmClass, JS_REALM_CLASS_INDEX, ecma_roots_class)                                            \
+    V(JSTaggedValue, WeakRefClass, WEAK_REF_CLASS_INDEX, ecma_roots_class)                                            \
     V(JSTaggedValue, LinkedHashSetClass, LINKED_HASH_SET_CLASS_INDEX, ecma_roots_class)                               \
     V(JSTaggedValue, WeakLinkedHashSetClass, WEAK_LINKED_HASH_SET_CLASS_INDEX, ecma_roots_class)                      \
     V(JSTaggedValue, LinkedHashMapClass, LINKED_HASH_MAP_CLASS_INDEX, ecma_roots_class)                               \
diff --git a/plugins/ecmascript/runtime/js_hclass.h b/plugins/ecmascript/runtime/js_hclass.h
index 234e53615c..baaefa56f3 100644
--- a/plugins/ecmascript/runtime/js_hclass.h
+++ b/plugins/ecmascript/runtime/js_hclass.h
@@ -92,6 +92,7 @@ class ProtoChangeDetails;
         JS_REG_EXP,  /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
         JS_SET,      /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
         JS_MAP,      /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
+        JS_WEAK_REF, /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
         JS_WEAK_MAP, /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
         JS_WEAK_SET, /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
         JS_DATE,     /* ///////////////////////////////////////////////////////////////////////////////////-PADDING */ \
@@ -553,6 +554,11 @@ public:
         return GetObjectType() == JSType::JS_MAP;
     }
 
+    bool IsJSWeakRef() const
+    {
+        return GetObjectType() == JSType::JS_WEAK_REF;
+    }
+
     bool IsJSWeakMap() const
     {
         return GetObjectType() == JSType::JS_WEAK_MAP;
diff --git a/plugins/ecmascript/runtime/js_tagged_value-inl.h b/plugins/ecmascript/runtime/js_tagged_value-inl.h
index f7eb1ae7c2..dfb6e74f36 100644
--- a/plugins/ecmascript/runtime/js_tagged_value-inl.h
+++ b/plugins/ecmascript/runtime/js_tagged_value-inl.h
@@ -651,6 +651,11 @@ inline bool JSTaggedValue::IsJSMap() const
     return IsHeapObject() && GetTaggedObject()->GetClass()->IsJSMap();
 }
 
+inline bool JSTaggedValue::IsJSWeakRef() const
+{
+    return IsHeapObject() && GetTaggedObject()->GetClass()->IsJSWeakRef();
+}
+
 inline bool JSTaggedValue::IsJSWeakMap() const
 {
     return IsHeapObject() && GetTaggedObject()->GetClass()->IsJSWeakMap();
diff --git a/plugins/ecmascript/runtime/js_tagged_value.h b/plugins/ecmascript/runtime/js_tagged_value.h
index 23c2f88c8c..c696716a32 100644
--- a/plugins/ecmascript/runtime/js_tagged_value.h
+++ b/plugins/ecmascript/runtime/js_tagged_value.h
@@ -215,6 +215,7 @@ public:
     // Type
     bool IsJSMap() const;
     bool IsJSSet() const;
+    bool IsJSWeakRef() const;
     bool IsJSWeakMap() const;
     bool IsJSWeakSet() const;
     bool IsJSRegExp() const;
diff --git a/plugins/ecmascript/runtime/js_weak_container.h b/plugins/ecmascript/runtime/js_weak_container.h
index d8aac384c6..f9536f87e6 100644
--- a/plugins/ecmascript/runtime/js_weak_container.h
+++ b/plugins/ecmascript/runtime/js_weak_container.h
@@ -21,6 +21,21 @@
 #include "plugins/ecmascript/runtime/js_tagged_value-inl.h"
 
 namespace panda::ecmascript {
+class JSWeakRef : public JSObject {
+public:
+    static JSWeakRef *Cast(ObjectHeader *object)
+    {
+        ASSERT(JSTaggedValue(object).IsJSWeakRef());
+        return static_cast<JSWeakRef *>(object);
+    }
+
+    static constexpr size_t REFERENT_OFFSET = JSObject::SIZE;
+    ACCESSORS(Referent, REFERENT_OFFSET, SIZE)
+
+    DECL_VISIT_OBJECT_FOR_JS_OBJECT(JSObject, REFERENT_OFFSET, SIZE)
+    DECL_DUMP()
+};
+
 class JSWeakMap : public JSObject {
 public:
     static JSWeakMap *Cast(ObjectHeader *object)
diff --git a/plugins/ecmascript/runtime/mem/ecma_reference_processor.cpp b/plugins/ecmascript/runtime/mem/ecma_reference_processor.cpp
index 4fd2f69b29..f55e999197 100644
--- a/plugins/ecmascript/runtime/mem/ecma_reference_processor.cpp
+++ b/plugins/ecmascript/runtime/mem/ecma_reference_processor.cpp
@@ -19,6 +19,7 @@
 #include "runtime/mem/gc/gc.h"
 #include "plugins/ecmascript/runtime/ecma_vm.h"
 #include "plugins/ecmascript/runtime/js_hclass.h"
+#include "plugins/ecmascript/runtime/js_weak_container.h"
 #include "plugins/ecmascript/runtime/linked_hash_table-inl.h"
 #include "plugins/ecmascript/runtime/mem/tagged_object.h"
 
@@ -67,6 +68,11 @@ bool EcmaReferenceProcessor::IsReference([[maybe_unused]] const BaseClass *base_
     };
     auto object_type = hcls->GetObjectType();
     switch (object_type) {
+        case panda::ecmascript::JSType::JS_WEAK_REF: {
+            panda::ecmascript::JSTaggedValue referent =
+                static_cast<const panda::ecmascript::JSWeakRef *>(ref)->GetReferent();
+            return referent.IsHeapObject() ? is_reference_checker(0, referent.GetRawHeapObject()) : false;
+        }
         case panda::ecmascript::JSType::LINKED_HASH_MAP:
             return EnumerateKeys(static_cast<const panda::ecmascript::LinkedHashMap *>(ref), is_reference_checker);
         case panda::ecmascript::JSType::LINKED_HASH_SET:
@@ -96,7 +102,14 @@ void EcmaReferenceProcessor::ProcessReferences([[maybe_unused]] bool concurrent,
         auto *hcls = panda::ecmascript::JSHClass::FromHClass(reference->ClassAddr<HClass>());
         ASSERT(hcls->IsWeakContainer());
         auto object_type = hcls->GetObjectType();
-        if (object_type == panda::ecmascript::JSType::LINKED_HASH_MAP) {
+        if (object_type == panda::ecmascript::JSType::JS_WEAK_REF) {
+            auto *ref = static_cast<panda::ecmascript::JSWeakRef *>(reference);
+            ASSERT(ref->GetReferent().IsHeapObject())
+            if (!gc_->IsMarked(ref->GetReferent().GetRawHeapObject())) {
+                ObjectAccessor::SetDynValueWithoutBarrier(ref, panda::ecmascript::JSWeakRef::REFERENT_OFFSET,
+                                                          panda::ecmascript::JSTaggedValue::Undefined().GetRawData());
+            }
+        } else if (object_type == panda::ecmascript::JSType::LINKED_HASH_MAP) {
             auto *map = static_cast<panda::ecmascript::LinkedHashMap *>(reference);
             auto map_handler = [this, map, thread](int index, ObjectHeader *key) {
                 if (!gc_->IsMarked(key)) {
diff --git a/plugins/ecmascript/runtime/object_factory.cpp b/plugins/ecmascript/runtime/object_factory.cpp
index dcb831ccfe..4df3a6bc72 100644
--- a/plugins/ecmascript/runtime/object_factory.cpp
+++ b/plugins/ecmascript/runtime/object_factory.cpp
@@ -165,6 +165,7 @@ void ObjectFactory::ObtainRootClass([[maybe_unused]] const JSHandle<GlobalEnv> &
 
     linkedHashMapClass_ = JSHClass::Cast(globalConst->GetLinkedHashMapClass().GetTaggedObject());
     linkedHashSetClass_ = JSHClass::Cast(globalConst->GetLinkedHashSetClass().GetTaggedObject());
+    weakRefClass_ = JSHClass::Cast(globalConst->GetWeakRefClass().GetTaggedObject());
     weakLinkedHashMapClass_ = JSHClass::Cast(globalConst->GetWeakLinkedHashMapClass().GetTaggedObject());
     weakLinkedHashSetClass_ = JSHClass::Cast(globalConst->GetWeakLinkedHashSetClass().GetTaggedObject());
 }
@@ -791,6 +792,9 @@ JSHandle<JSObject> ObjectFactory::NewJSObjectByConstructor(const JSHandle<JSFunc
             case JSType::JS_MAP:
                 JSMap::Cast(*obj)->SetLinkedMap(thread_, JSTaggedValue::Undefined());
                 break;
+            case JSType::JS_WEAK_REF:
+                JSWeakRef::Cast(*obj)->SetReferent(JSTaggedValue::Undefined());
+                break;
             case JSType::JS_WEAK_MAP:
                 JSWeakMap::Cast(*obj)->SetLinkedMap(thread_, JSTaggedValue::Undefined());
                 break;
@@ -1931,6 +1935,17 @@ JSHandle<TaggedQueue> ObjectFactory::GetEmptyTaggedQueue() const
     return JSHandle<TaggedQueue>(env->GetEmptyTaggedQueue());
 }
 
+JSHandle<JSWeakRef> ObjectFactory::NewWeakRef(const JSHandle<JSObject> &referent)
+{
+    auto header = heapHelper_.AllocateYoungGenerationOrHugeObject(weakRefClass_);
+    if (header == nullptr) {
+        return JSHandle<JSWeakRef>();
+    }
+    JSHandle<JSWeakRef> ref(thread_, header);
+    ref->SetReferent(referent.GetTaggedValue());
+    return ref;
+}
+
 JSHandle<JSSetIterator> ObjectFactory::NewJSSetIterator(const JSHandle<JSSet> &set, IterationKind kind)
 {
     JSHandle<GlobalEnv> env = vm_->GetGlobalEnv();
diff --git a/plugins/ecmascript/runtime/object_factory.h b/plugins/ecmascript/runtime/object_factory.h
index d766a832ec..b8917e5b8f 100644
--- a/plugins/ecmascript/runtime/object_factory.h
+++ b/plugins/ecmascript/runtime/object_factory.h
@@ -55,6 +55,7 @@ class TaggedQueue;
 class JSForInIterator;
 class JSSet;
 class JSMap;
+class JSWeakRef;
 class JSRegExp;
 class JSSetIterator;
 class JSMapIterator;
@@ -283,6 +284,8 @@ public:
 
     JSHandle<TaggedQueue> GetEmptyTaggedQueue() const;
 
+    JSHandle<JSWeakRef> NewWeakRef(const JSHandle<JSObject> &referent);
+
     JSHandle<JSSetIterator> NewJSSetIterator(const JSHandle<JSSet> &set, IterationKind kind);
 
     JSHandle<JSMapIterator> NewJSMapIterator(const JSHandle<JSMap> &map, IterationKind kind);
@@ -444,6 +447,7 @@ private:
     JSHClass *classInfoExtractorHClass_ {nullptr};
     JSHClass *linkedHashMapClass_ {nullptr};
     JSHClass *linkedHashSetClass_ {nullptr};
+    JSHClass *weakRefClass_ {nullptr};
     JSHClass *weakLinkedHashMapClass_ {nullptr};
     JSHClass *weakLinkedHashSetClass_ {nullptr};
 
diff --git a/plugins/ecmascript/runtime/runtime_call_id.h b/plugins/ecmascript/runtime/runtime_call_id.h
index 766deb3753..68aa9864bb 100644
--- a/plugins/ecmascript/runtime/runtime_call_id.h
+++ b/plugins/ecmascript/runtime/runtime_call_id.h
@@ -543,6 +543,8 @@ namespace panda::ecmascript {
     V(TypedArray, Subarray)                   \
     V(TypedArray, Values)                     \
     V(TypedArray, ToStringTag)                \
+    V(WeakRef, Constructor)                   \
+    V(WeakRef, Deref)                         \
     V(WeakMap, Constructor)                   \
     V(WeakMap, Delete)                        \
     V(WeakMap, Get)                           \
diff --git a/plugins/ecmascript/runtime/runtime_sources.gn b/plugins/ecmascript/runtime/runtime_sources.gn
index f71db18352..d494556bb6 100644
--- a/plugins/ecmascript/runtime/runtime_sources.gn
+++ b/plugins/ecmascript/runtime/runtime_sources.gn
@@ -55,6 +55,7 @@ srcs = [
   "builtins/builtins_string_iterator.cpp",
   "builtins/builtins_symbol.cpp",
   "builtins/builtins_typedarray.cpp",
+  "builtins/builtins_weak_ref.cpp",
   "builtins/builtins_weak_map.cpp",
   "builtins/builtins_weak_set.cpp",
   "class_linker/panda_file_translator.cpp",
-- 
2.17.1

