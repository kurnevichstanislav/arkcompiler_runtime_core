From 590bf9afdf526119e8a18f9a98e33669f8483ea9 Mon Sep 17 00:00:00 2001
From: Pavel Andrianov <>
Date: Thu, 19 May 2022 20:46:32 +0800
Subject: [PATCH 46/52] Refactoring of thread_manager interface

TicketNo:_internal_
Description:To avoid different templates for single- and multithreading thread manager we refactored
it and extracted a generic interface and two two implementations: for single thread mode
(script languages) and multi thread mode (java)

Team:ARK
Feature or Bugfix:Bugfix
Binary Source:No
PrivateCode(Yes/No):No

Change-Id: I0e8a61ab31f8fb4fcc780e9f30203597417c17d4
---
 plugins/ecmascript/runtime/ecma_vm.cpp        | 30 +--------
 plugins/ecmascript/runtime/ecma_vm.h          |  7 +-
 runtime/BUILD.gn                              |  2 +-
 runtime/CMakeLists.txt                        |  2 +-
 runtime/core/core_vm.cpp                      |  2 +-
 runtime/mem/allocator.cpp                     |  7 +-
 runtime/mem/gc/g1/g1-allocator.cpp            |  9 +--
 runtime/mem/gc/g1/g1-gc.cpp                   | 10 +--
 runtime/mem/gc/gc_root.cpp                    | 18 ++----
 runtime/mem/gc/lang/gc_lang.cpp               | 19 ++----
 runtime/monitor.cpp                           |  3 +-
 ...read_manager.cpp => mt_thread_manager.cpp} | 40 ++++++------
 runtime/single_thread_manager.h               | 64 +++++++++++++++++++
 runtime/thread.cpp                            | 15 +++--
 runtime/thread_manager.h                      | 54 ++++++++++++----
 15 files changed, 158 insertions(+), 124 deletions(-)
 rename runtime/{thread_manager.cpp => mt_thread_manager.cpp} (90%)
 create mode 100644 runtime/single_thread_manager.h

diff --git a/plugins/ecmascript/runtime/ecma_vm.cpp b/plugins/ecmascript/runtime/ecma_vm.cpp
index 5a18f23be7..d216d2abc3 100644
--- a/plugins/ecmascript/runtime/ecma_vm.cpp
+++ b/plugins/ecmascript/runtime/ecma_vm.cpp
@@ -83,34 +83,6 @@ namespace panda::ecmascript {
 static const std::string_view ENTRY_POINTER = "_GLOBAL::func_main_0";
 JSRuntimeOptions EcmaVM::options_;  // NOLINT(fuchsia-statically-constructed-objects)
 
-void EcmaRendezvous::SafepointBegin()
-{
-    ASSERT(!Locks::mutator_lock->HasLock());
-    LOG(DEBUG, GC) << "Rendezvous: SafepointBegin";
-    Thread *current = Thread::GetCurrent();
-    ManagedThread *main_thread = current->GetVM()->GetAssociatedThread();
-
-    if (current != main_thread) {
-        main_thread->SuspendImpl(true);
-    }
-    // Acquire write MutatorLock
-    Locks::mutator_lock->WriteLock();
-}
-
-void EcmaRendezvous::SafepointEnd()
-{
-    ASSERT(Locks::mutator_lock->HasLock());
-    LOG(DEBUG, GC) << "Rendezvous: SafepointEnd";
-    // Release write MutatorLock
-    Locks::mutator_lock->Unlock();
-    Thread *current = Thread::GetCurrent();
-    ManagedThread *main_thread = current->GetVM()->GetAssociatedThread();
-    if (current != main_thread) {
-        main_thread->ResumeImpl(true);
-    }
-    LOG(DEBUG, GC) << "Rendezvous: SafepointEnd exit";
-}
-
 // Create MemoryManager by RuntimeOptions
 static mem::MemoryManager *CreateMM(const LanguageContext &ctx, mem::InternalAllocatorPtr internal_allocator,
                                     const RuntimeOptions &options)
@@ -188,7 +160,7 @@ EcmaVM::EcmaVM(JSRuntimeOptions options)
     options_ = std::move(options);
     icEnable_ = options_.IsIcEnable();
     optionalLogEnabled_ = options_.IsEnableOptionalLog();
-    rendezvous_ = chunk_.New<EcmaRendezvous>();
+    rendezvous_ = chunk_.New<Rendezvous>();
     snapshotSerializeEnable_ = options_.IsSnapshotSerializeEnabled();
     if (!snapshotSerializeEnable_) {
         snapshotDeserializeEnable_ = options_.IsSnapshotDeserializeEnabled();
diff --git a/plugins/ecmascript/runtime/ecma_vm.h b/plugins/ecmascript/runtime/ecma_vm.h
index f82ffa0ffc..ff2d3f4d80 100644
--- a/plugins/ecmascript/runtime/ecma_vm.h
+++ b/plugins/ecmascript/runtime/ecma_vm.h
@@ -81,11 +81,6 @@ using HostPromiseRejectionTracker = void (*)(const EcmaVM *vm, const JSHandle<JS
                                              const PromiseRejectionEvent operation, void *data);
 using PromiseRejectCallback = void (*)(void *info);
 
-class EcmaRendezvous : public Rendezvous {
-    void SafepointBegin() ACQUIRE(*Locks::mutator_lock) override;
-    void SafepointEnd() RELEASE(*Locks::mutator_lock) override;
-};
-
 class EcmaVM final : public PandaVM {
     using PtJSExtractor = tooling::ecmascript::PtJSExtractor;
 
@@ -523,7 +518,7 @@ private:
     mem::MemoryManager *mm_ {nullptr};
     PandaUniquePtr<panda::mem::ReferenceProcessor> ecma_reference_processor_;
 
-    EcmaRendezvous *rendezvous_ {nullptr};
+    Rendezvous *rendezvous_ {nullptr};
     bool isTestMode_ {false};
 
     // VM startup states.
diff --git a/runtime/BUILD.gn b/runtime/BUILD.gn
index ba1da8cdb8..cad3757fc9 100644
--- a/runtime/BUILD.gn
+++ b/runtime/BUILD.gn
@@ -254,7 +254,7 @@ source_set("libarkruntime_set_static") {
     "stack_walker.cpp",
     "string_table.cpp",
     "thread.cpp",
-    "thread_manager.cpp",
+    "mt_thread_manager.cpp",
     "time_utils.cpp",
     "timing.cpp",
     "tooling/debug_inf.cpp",
diff --git a/runtime/CMakeLists.txt b/runtime/CMakeLists.txt
index fc2a20df3b..b924888aeb 100644
--- a/runtime/CMakeLists.txt
+++ b/runtime/CMakeLists.txt
@@ -97,7 +97,7 @@ set(SOURCES
     runtime_controller.cpp
     string_table.cpp
     thread.cpp
-    thread_manager.cpp
+    mt_thread_manager.cpp
     lock_order_graph.cpp
     time_utils.cpp
     timing.cpp
diff --git a/runtime/core/core_vm.cpp b/runtime/core/core_vm.cpp
index 04d9ebdd83..885a3d5310 100644
--- a/runtime/core/core_vm.cpp
+++ b/runtime/core/core_vm.cpp
@@ -86,7 +86,7 @@ PandaCoreVM::PandaCoreVM(Runtime *runtime, const RuntimeOptions &options, mem::M
     string_table_ = allocator->New<StringTable>();
     monitor_pool_ = allocator->New<MonitorPool>(allocator);
     reference_processor_ = allocator->New<mem::EmptyReferenceProcessor>();
-    thread_manager_ = allocator->New<ThreadManager>(allocator);
+    thread_manager_ = allocator->New<MTThreadManager>(allocator);
     rendezvous_ = allocator->New<Rendezvous>();
 }
 
diff --git a/runtime/mem/allocator.cpp b/runtime/mem/allocator.cpp
index dc689a0e0e..5c56df7753 100644
--- a/runtime/mem/allocator.cpp
+++ b/runtime/mem/allocator.cpp
@@ -523,12 +523,7 @@ void ObjectAllocatorGen<MTMode>::ResetYoungAllocator()
         thread->ClearTLAB();
         return true;
     };
-    // NOLINTNEXTLINE(readability-braces-around-statements)
-    if constexpr (MTMode == MT_MODE_MULTI) {
-        Thread::GetCurrent()->GetVM()->GetThreadManager()->EnumerateThreads(thread_callback);
-    } else {  // NOLINT(readability-misleading-indentation)
-        thread_callback(Thread::GetCurrent()->GetVM()->GetAssociatedThread());
-    }
+    Thread::GetCurrent()->GetVM()->GetThreadManager()->EnumerateThreads(thread_callback);
     young_gen_allocator_->Reset();
 }
 
diff --git a/runtime/mem/gc/g1/g1-allocator.cpp b/runtime/mem/gc/g1/g1-allocator.cpp
index b5b4e08202..c356b86be5 100644
--- a/runtime/mem/gc/g1/g1-allocator.cpp
+++ b/runtime/mem/gc/g1/g1-allocator.cpp
@@ -369,14 +369,7 @@ void ObjectAllocatorG1<MTMode>::ResetYoungAllocator()
         thread->ClearTLAB();
         return true;
     };
-    // NOLINTNEXTLINE(readability-braces-around-statements)
-    if constexpr (MTMode == MT_MODE_MULTI) {
-        Thread::GetCurrent()->GetVM()->GetThreadManager()->EnumerateThreads(callback);
-    } else if (MTMode == MT_MODE_SINGLE) {  // NOLINT(readability-misleading-indentation)
-        callback(Thread::GetCurrent()->GetVM()->GetAssociatedThread());
-    } else {
-        UNREACHABLE();
-    }
+    Thread::GetCurrent()->GetVM()->GetThreadManager()->EnumerateThreads(callback);
     object_allocator_->ResetAllSpecificRegions<RegionFlag::IS_EDEN>();
 }
 
diff --git a/runtime/mem/gc/g1/g1-gc.cpp b/runtime/mem/gc/g1/g1-gc.cpp
index 7966fd1ad1..1de7f0eaa7 100644
--- a/runtime/mem/gc/g1/g1-gc.cpp
+++ b/runtime/mem/gc/g1/g1-gc.cpp
@@ -1402,15 +1402,7 @@ void G1GC<LanguageConfig>::DrainSatb(GCAdaptiveStack *object_stack)
         pre_buff->clear();
         return true;
     };
-    // NOLINTNEXTLINE(readability-braces-around-statements)
-    if constexpr (LanguageConfig::MT_MODE == MT_MODE_MULTI) {
-        Thread::GetCurrent()->GetVM()->GetThreadManager()->EnumerateThreads(
-            callback, static_cast<unsigned int>(EnumerationFlag::ALL));
-    } else if (LanguageConfig::MT_MODE == MT_MODE_SINGLE) {  // NOLINT(readability-misleading-indentation)
-        callback(Thread::GetCurrent()->GetVM()->GetAssociatedThread());
-    } else {
-        UNREACHABLE();
-    }
+    Thread::GetCurrent()->GetVM()->GetThreadManager()->EnumerateThreads(callback);
 
     // Process satb buffers of the terminated threads
     os::memory::LockHolder lock(satb_and_newobj_buf_lock_);
diff --git a/runtime/mem/gc/gc_root.cpp b/runtime/mem/gc/gc_root.cpp
index e8ff48a327..f0d6d95fd9 100644
--- a/runtime/mem/gc/gc_root.cpp
+++ b/runtime/mem/gc/gc_root.cpp
@@ -170,11 +170,7 @@ void RootManager<LanguageConfig>::VisitLocalRoots(const GCRootVisitor &gc_root_v
         }
         return true;
     };
-    if constexpr (LanguageConfig::MT_MODE == MT_MODE_MULTI) {  // NOLINT
-        vm_->GetThreadManager()->EnumerateThreads(thread_visitor);
-    } else {  // NOLINT
-        thread_visitor(vm_->GetAssociatedThread());
-    }
+    vm_->GetThreadManager()->EnumerateThreads(thread_visitor);
 }
 
 template <class LanguageConfig>
@@ -271,14 +267,10 @@ template <class LanguageConfig>
 void RootManager<LanguageConfig>::UpdateThreadLocals()
 {
     LOG(DEBUG, GC) << "=== ThreadLocals Update moved. BEGIN ===";
-    if constexpr (LanguageConfig::MT_MODE == MT_MODE_MULTI) {  // NOLINT
-        vm_->GetThreadManager()->EnumerateThreads([](MTManagedThread *thread) {
-            thread->UpdateGCRoots();
-            return true;
-        });
-    } else {  // NOLINT
-        vm_->GetAssociatedThread()->UpdateGCRoots();
-    }
+    vm_->GetThreadManager()->EnumerateThreads([](MTManagedThread *thread) {
+        thread->UpdateGCRoots();
+        return true;
+    });
     LOG(DEBUG, GC) << "=== ThreadLocals Update moved. END ===";
 }
 
diff --git a/runtime/mem/gc/lang/gc_lang.cpp b/runtime/mem/gc/lang/gc_lang.cpp
index 22ccade0a4..41bb6bdd6e 100644
--- a/runtime/mem/gc/lang/gc_lang.cpp
+++ b/runtime/mem/gc/lang/gc_lang.cpp
@@ -48,12 +48,7 @@ void GCLang<LanguageConfig>::ClearLocalInternalAllocatorPools()
         InternalAllocator<>::RemoveFreePoolsForLocalInternalAllocator(thread->GetLocalInternalAllocator());
         return true;
     };
-    // NOLINTNEXTLINE(readability-braces-around-statements)
-    if constexpr (LanguageConfig::MT_MODE == MT_MODE_MULTI) {
-        GetPandaVm()->GetThreadManager()->EnumerateThreads(cleaner);
-    } else {  // NOLINT(readability-misleading-indentation)
-        cleaner(GetPandaVm()->GetAssociatedThread());
-    }
+    GetPandaVm()->GetThreadManager()->EnumerateThreads(cleaner);
 }
 
 template <class LanguageConfig>
@@ -78,13 +73,11 @@ void GCLang<LanguageConfig>::CommonUpdateRefsToMovedObjects()
     trace::ScopedTrace scoped_trace(__FUNCTION__);
 
     // Update refs in vregs
-    if constexpr (LanguageConfig::MT_MODE == MT_MODE_SINGLE) {  // NOLINT
-        UpdateRefsInVRegs(GetPandaVm()->GetAssociatedThread());
-    } else {  // NOLINT
-        GetPandaVm()->GetThreadManager()->EnumerateThreads([this](ManagedThread *thread) {
-            UpdateRefsInVRegs(thread);
-            return true;
-        });
+    GetPandaVm()->GetThreadManager()->EnumerateThreads([this](ManagedThread *thread) {
+        UpdateRefsInVRegs(thread);
+        return true;
+    });
+    if constexpr (LanguageConfig::MT_MODE != MT_MODE_SINGLE) {  // NOLINT
         // Update refs inside monitors
         GetPandaVm()->GetMonitorPool()->EnumerateMonitors([this](Monitor *monitor) {
             ObjectHeader *object_header = monitor->GetObject();
diff --git a/runtime/monitor.cpp b/runtime/monitor.cpp
index fd927c538a..c503e8d7b3 100644
--- a/runtime/monitor.cpp
+++ b/runtime/monitor.cpp
@@ -106,7 +106,8 @@ void Monitor::InflateThinLock(MTManagedThread *thread, [[maybe_unused]] const VM
     os::thread::ThreadId owner_thread_id = mark.GetThreadId();
     {
         ScopedChangeThreadStatus sts(thread, ThreadStatus::IS_WAITING_INFLATION);
-        owner = thread->GetVM()->GetThreadManager()->SuspendAndWaitThreadByInternalThreadId(owner_thread_id);
+        auto thread_manager = reinterpret_cast<MTThreadManager *>(thread->GetVM()->GetThreadManager());
+        owner = thread_manager->SuspendAndWaitThreadByInternalThreadId(owner_thread_id);
     }
     thread->SetEnterMonitorObject(nullptr);
     thread->SetWaitingMonitorOldStatus(ThreadStatus::FINISHED);
diff --git a/runtime/thread_manager.cpp b/runtime/mt_thread_manager.cpp
similarity index 90%
rename from runtime/thread_manager.cpp
rename to runtime/mt_thread_manager.cpp
index 94b585128c..07512d843f 100644
--- a/runtime/thread_manager.cpp
+++ b/runtime/mt_thread_manager.cpp
@@ -27,18 +27,18 @@
 
 namespace panda {
 
-ThreadManager::ThreadManager(mem::InternalAllocatorPtr allocator) : threads_(allocator->Adapter())
+MTThreadManager::MTThreadManager(mem::InternalAllocatorPtr allocator) : threads_(allocator->Adapter())
 {
     last_id_ = 0;
     pending_threads_ = 0;
 }
 
-ThreadManager::~ThreadManager()
+MTThreadManager::~MTThreadManager()
 {
     threads_.clear();
 }
 
-uint32_t ThreadManager::GetInternalThreadId()
+uint32_t MTThreadManager::GetInternalThreadId()
 {
     os::memory::LockHolder lock(ids_lock_);
     for (size_t i = 0; i < internal_thread_ids_.size(); i++) {
@@ -52,7 +52,7 @@ uint32_t ThreadManager::GetInternalThreadId()
     UNREACHABLE();
 }
 
-void ThreadManager::RemoveInternalThreadId(uint32_t id)
+void MTThreadManager::RemoveInternalThreadId(uint32_t id)
 {
     id--;  // 0 is reserved as uninitialized value.
     os::memory::LockHolder lock(ids_lock_);
@@ -60,7 +60,7 @@ void ThreadManager::RemoveInternalThreadId(uint32_t id)
     internal_thread_ids_.reset(id);
 }
 
-MTManagedThread *ThreadManager::GetThreadByInternalThreadIdWithLockHeld(uint32_t thread_id)
+MTManagedThread *MTThreadManager::GetThreadByInternalThreadIdWithLockHeld(uint32_t thread_id)
 {
     // Do not optimize with std::find_if - sometimes there are problems with incorrect memory accesses
     for (auto thread : threads_) {
@@ -71,7 +71,7 @@ MTManagedThread *ThreadManager::GetThreadByInternalThreadIdWithLockHeld(uint32_t
     return nullptr;
 }
 
-bool ThreadManager::DeregisterSuspendedThreads()
+bool MTThreadManager::DeregisterSuspendedThreads()
 {
     auto current = MTManagedThread::GetCurrent();
     auto i = threads_.begin();
@@ -112,7 +112,7 @@ bool ThreadManager::DeregisterSuspendedThreads()
     return threads_.size() == 1;
 }
 
-void ThreadManager::DecreaseCountersForThread(MTManagedThread *thread)
+void MTThreadManager::DecreaseCountersForThread(MTManagedThread *thread)
 {
     if (thread->IsDaemon()) {
         daemon_threads_count_--;
@@ -122,7 +122,7 @@ void ThreadManager::DecreaseCountersForThread(MTManagedThread *thread)
     threads_count_--;
 }
 
-bool ThreadManager::StopThreadsOnDeadlock(MTManagedThread *current)
+bool MTThreadManager::StopThreadsOnDeadlock(MTManagedThread *current)
 {
     if (!LockOrderGraph::CheckForTerminationLoops(threads_, daemon_threads_, current)) {
         LOG(DEBUG, RUNTIME) << "Deadlock with daemon threads was not confirmed";
@@ -143,7 +143,7 @@ bool ThreadManager::StopThreadsOnDeadlock(MTManagedThread *current)
     return true;
 }
 
-void ThreadManager::WaitForDeregistration()
+void MTThreadManager::WaitForDeregistration()
 {
     trace::ScopedTrace scoped_trace(__FUNCTION__);
     {
@@ -179,7 +179,7 @@ void ThreadManager::WaitForDeregistration()
     Runtime::GetCurrent()->SetDaemonMemoryLeakThreshold(daemon_threads_.size() * threshold);
 }
 
-void ThreadManager::StopDaemonThreads() REQUIRES(thread_lock_)
+void MTThreadManager::StopDaemonThreads() REQUIRES(thread_lock_)
 {
     trace::ScopedTrace scoped_trace(__FUNCTION__);
     for (auto thread : threads_) {
@@ -192,19 +192,19 @@ void ThreadManager::StopDaemonThreads() REQUIRES(thread_lock_)
     suspend_new_count_++;
 }
 
-int ThreadManager::GetThreadsCount()
+int MTThreadManager::GetThreadsCount()
 {
     return threads_count_;
 }
 
 #ifndef NDEBUG
-uint32_t ThreadManager::GetAllRegisteredThreadsCount()
+uint32_t MTThreadManager::GetAllRegisteredThreadsCount()
 {
     return registered_threads_count_;
 }
 #endif  // NDEBUG
 
-void ThreadManager::SuspendAllThreads()
+void MTThreadManager::SuspendAllThreads()
 {
     trace::ScopedTrace scoped_trace("Suspending mutator threads");
     auto cur_thread = MTManagedThread::GetCurrent();
@@ -218,7 +218,7 @@ void ThreadManager::SuspendAllThreads()
     suspend_new_count_++;
 }
 
-bool ThreadManager::IsRunningThreadExist()
+bool MTThreadManager::IsRunningThreadExist()
 {
     auto cur_thread = MTManagedThread::GetCurrent();
     os::memory::LockHolder lock(thread_lock_);
@@ -235,7 +235,7 @@ bool ThreadManager::IsRunningThreadExist()
     return is_exists;
 }
 
-void ThreadManager::ResumeAllThreads()
+void MTThreadManager::ResumeAllThreads()
 {
     trace::ScopedTrace scoped_trace("Resuming mutator threads");
     auto cur_thread = MTManagedThread::GetCurrent();
@@ -251,7 +251,7 @@ void ThreadManager::ResumeAllThreads()
     });
 }
 
-bool ThreadManager::UnregisterExitedThread(MTManagedThread *thread)
+bool MTThreadManager::UnregisterExitedThread(MTManagedThread *thread)
 {
     ASSERT(MTManagedThread::GetCurrent() == thread);
     {
@@ -271,7 +271,7 @@ bool ThreadManager::UnregisterExitedThread(MTManagedThread *thread)
         LOG(DEBUG, RUNTIME) << "Stopping thread " << thread->GetId();
         thread->UpdateStatus(ThreadStatus::FINISHED);
         // Do not delete main thread, Runtime::GetMainThread is expected to always return valid object
-        if (thread == main_thread_) {
+        if (thread == GetMainThread()) {
             return false;
         }
 
@@ -293,12 +293,12 @@ bool ThreadManager::UnregisterExitedThread(MTManagedThread *thread)
     }
 }
 
-void ThreadManager::RegisterSensitiveThread() const
+void MTThreadManager::RegisterSensitiveThread() const
 {
     LOG(INFO, RUNTIME) << __func__ << " is an empty implementation now.";
 }
 
-void ThreadManager::DumpUnattachedThreads(std::ostream &os)
+void MTThreadManager::DumpUnattachedThreads(std::ostream &os)
 {
     os::native_stack::DumpUnattachedThread dump;
     dump.InitKernelTidLists();
@@ -309,7 +309,7 @@ void ThreadManager::DumpUnattachedThreads(std::ostream &os)
     dump.Dump(os, Runtime::GetCurrent()->IsDumpNativeCrash(), Runtime::GetCurrent()->GetUnwindStackFn());
 }
 
-MTManagedThread *ThreadManager::SuspendAndWaitThreadByInternalThreadId(uint32_t thread_id)
+MTManagedThread *MTThreadManager::SuspendAndWaitThreadByInternalThreadId(uint32_t thread_id)
 {
     static constexpr uint32_t YIELD_ITERS = 500;
     // NB! Expected to be called in registered thread, change implementation if this function used elsewhere
diff --git a/runtime/single_thread_manager.h b/runtime/single_thread_manager.h
new file mode 100644
index 0000000000..d166363ecd
--- /dev/null
+++ b/runtime/single_thread_manager.h
@@ -0,0 +1,64 @@
+/**
+ * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ * http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+#ifndef PANDA_RUNTIME_SINGLE_THREAD_MANAGER_H
+#define PANDA_RUNTIME_SINGLE_THREAD_MANAGER_H
+
+#include "runtime/thread_manager.h"
+
+namespace openjdkjvmti {
+class TiThread;
+}  // namespace openjdkjvmti
+
+namespace panda {
+
+class SingleThreadManager : public ThreadManager {
+public:
+    NO_COPY_SEMANTIC(SingleThreadManager);
+    NO_MOVE_SEMANTIC(SingleThreadManager);
+
+    explicit SingleThreadManager() = default;
+
+    ~SingleThreadManager() = default;
+
+    template <class Callback>
+    void EnumerateThreads(
+        const Callback &cb, [[maybe_unused]] unsigned int mask = static_cast<unsigned int>(EnumerationFlag::ALL),
+        [[maybe_unused]] unsigned int xor_mask = static_cast<unsigned int>(EnumerationFlag::NONE)) const
+    {
+        cb(GetMainThread());
+    }
+
+    void WaitForDeregistration() {}
+
+    void SuspendAllThreads() {
+        Thread *current = Thread::GetCurrent();
+        ManagedThread *main_thread = current->GetVM()->GetAssociatedThread();
+        if (current != GetMainThread()) {
+            main_thread->SuspendImpl(true);
+        }
+    }
+
+    void ResumeAllThreads() {
+        Thread *current = Thread::GetCurrent();
+        ManagedThread *main_thread = current->GetVM()->GetAssociatedThread();
+        if (current != main_thread) {
+            main_thread->ResumeImpl(true);
+        }
+    }
+};
+
+}  // namespace panda
+
+#endif  // PANDA_RUNTIME_SINGLE_THREAD_MANAGER_H
diff --git a/runtime/thread.cpp b/runtime/thread.cpp
index 5af915a82c..2b1d0100b7 100644
--- a/runtime/thread.cpp
+++ b/runtime/thread.cpp
@@ -559,7 +559,8 @@ MTManagedThread::MTManagedThread(ThreadId id, mem::InternalAllocatorPtr allocato
       entering_monitor_(nullptr)
 {
     ASSERT(panda_vm != nullptr);
-    internal_id_ = GetVM()->GetThreadManager()->GetInternalThreadId();
+    auto thread_manager = reinterpret_cast<MTThreadManager *>(GetVM()->GetThreadManager());
+    internal_id_ = thread_manager->GetInternalThreadId();
 
     auto ext = Runtime::GetCurrent()->GetClassLinker()->GetExtension(GetThreadLang());
     if (ext != nullptr) {
@@ -574,7 +575,8 @@ MTManagedThread::MTManagedThread(ThreadId id, mem::InternalAllocatorPtr allocato
 MTManagedThread::~MTManagedThread()
 {
     ASSERT(internal_id_ != 0);
-    GetVM()->GetThreadManager()->RemoveInternalThreadId(internal_id_);
+    auto thread_manager = reinterpret_cast<MTThreadManager *>(GetVM()->GetThreadManager());
+    thread_manager->RemoveInternalThreadId(internal_id_);
 }
 
 void ManagedThread::PushLocalObject(ObjectHeader **object_header)
@@ -654,7 +656,8 @@ void MTManagedThread::ProcessCreatedThread()
     ManagedThread::SetCurrent(this);
     // Runtime takes ownership of the thread
     trace::ScopedTrace scoped_trace2("ThreadManager::RegisterThread");
-    GetVM()->GetThreadManager()->RegisterThread(this);
+    auto thread_manager = reinterpret_cast<MTThreadManager *>(GetVM()->GetThreadManager());
+    thread_manager->RegisterThread(this);
     NativeCodeBegin();
 }
 
@@ -740,7 +743,8 @@ void MTManagedThread::VisitGCRoots(const ObjectVisitor &cb)
 void MTManagedThread::SetDaemon()
 {
     is_daemon_ = true;
-    GetVM()->GetThreadManager()->AddDaemonThread();
+    auto thread_manager = reinterpret_cast<MTThreadManager *>(GetVM()->GetThreadManager());
+    thread_manager->AddDaemonThread();
     SetThreadPriority(MIN_PRIORITY);
 }
 
@@ -799,7 +803,8 @@ void MTManagedThread::Destroy()
         runtime->GetNotificationManager()->ThreadEndEvent(this);
     }
 
-    if (GetVM()->GetThreadManager()->UnregisterExitedThread(this)) {
+    auto thread_manager = reinterpret_cast<MTThreadManager *>(GetVM()->GetThreadManager());
+    if (thread_manager->UnregisterExitedThread(this)) {
         // Clear current_thread only if unregistration was successfull
         ManagedThread::SetCurrent(nullptr);
     }
diff --git a/runtime/thread_manager.h b/runtime/thread_manager.h
index 2f7d3e886e..bfd85e0712 100644
--- a/runtime/thread_manager.h
+++ b/runtime/thread_manager.h
@@ -47,12 +47,50 @@ public:
     NO_COPY_SEMANTIC(ThreadManager);
     NO_MOVE_SEMANTIC(ThreadManager);
 
+    ThreadManager() = default;
+    virtual ~ThreadManager() = default;
+
+    // There is no virtual template functions
+    template <class Callback>
+    void EnumerateThreads(
+        [[maybe_unused]] const Callback &cb,
+        [[maybe_unused]] unsigned int mask = static_cast<unsigned int>(EnumerationFlag::ALL),
+        [[maybe_unused]] unsigned int xor_mask = static_cast<unsigned int>(EnumerationFlag::NONE)) const
+    {
+    }
+
+    void SetMainThread(ManagedThread *thread)
+    {
+        main_thread_ = thread;
+    }
+
+    virtual void WaitForDeregistration() {}
+
+    virtual void SuspendAllThreads() {}
+    virtual void ResumeAllThreads() {}
+
+protected:
+    ManagedThread *GetMainThread() const
+    {
+        return main_thread_;
+    }
+
+private:
+    ManagedThread *main_thread_ {nullptr};
+    friend class openjdkjvmti::TiThread;
+};
+
+class MTThreadManager : public ThreadManager {
+public:
+    NO_COPY_SEMANTIC(MTThreadManager);
+    NO_MOVE_SEMANTIC(MTThreadManager);
+
     // For performance reasons don't exceed specified amount of bits.
     static constexpr size_t MAX_INTERNAL_THREAD_ID = std::min(0xffffU, ManagedThread::MAX_INTERNAL_THREAD_ID);
 
-    explicit ThreadManager(mem::InternalAllocatorPtr allocator);
+    explicit MTThreadManager(mem::InternalAllocatorPtr allocator);
 
-    virtual ~ThreadManager();
+    ~MTThreadManager() override;
 
     template <class Callback>
     void EnumerateThreads(const Callback &cb, unsigned int mask = static_cast<unsigned int>(EnumerationFlag::ALL),
@@ -156,10 +194,10 @@ public:
     uint32_t GetAllRegisteredThreadsCount();
 #endif  // NDEBUG
 
-    void WaitForDeregistration();
+    void WaitForDeregistration() override;
 
-    void SuspendAllThreads();
-    void ResumeAllThreads();
+    void SuspendAllThreads() override;
+    void ResumeAllThreads() override;
 
     uint32_t GetInternalThreadId();
 
@@ -179,11 +217,6 @@ public:
         return &thread_lock_;
     }
 
-    void SetMainThread(ManagedThread *thread)
-    {
-        main_thread_ = thread;
-    }
-
 private:
     bool HasNoActiveThreads() const REQUIRES(thread_lock_)
     {
@@ -253,7 +286,6 @@ private:
     }
 
     mutable os::memory::Mutex thread_lock_;
-    ManagedThread *main_thread_ {nullptr};
     // Counter used to suspend newly created threads after SuspendAllThreads/SuspendDaemonThreads
     uint32_t suspend_new_count_ GUARDED_BY(thread_lock_) = 0;
     // We should delete only finished thread structures, so call delete explicitly on finished threads
-- 
2.17.1

